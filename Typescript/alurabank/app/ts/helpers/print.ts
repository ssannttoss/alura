import { Printable } from "../models/index";

export function print(...entities: Printable[]) {
    entities.forEach(entity => entity.print());
}