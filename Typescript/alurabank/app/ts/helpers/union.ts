/**
 * Considerações sobre o uso de Type Guards: Apesar de ser um recurso da linguagem, 
 * essa estratégia remete à programação procedural pois envolve uma sucessão de if's 
 * para detectar o tipo dos elementos. É por este motivo que não foi utilizado em nosso 
 * projeto e demos preferência ao polimorfismo.
 * https://cursos.alura.com.br/course/typescript-parte2/task/28081
 */

/**
 * https://cursos.alura.com.br/course/typescript-parte2/task/28082
 */
// criando o alias!
type MeuToken = string |  number;

// function processaToken(token: string | number) {
function processaToken(token: MeuToken) {
    // muda o dígito 2 por X!
    // return token.replace(/2/g,'X');

   if(typeof(token) === "string") {
        // typescript entende que é o tipo string e faz autocomplete para este tipo. A função replace só existe em string
        return token.replace(/2/g,'X');
    } else {
        // toFixed só existe em mumber!
        return token.toFixed().replace(/2/g,'X');
    } 
}

// compila
const tokenProcessado1 = processaToken('1234');
// compila
const tokenProcessado2 = processaToken(1234);