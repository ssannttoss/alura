export function logExecutionTime(timeLimit: number = 500) {
    return function(target: any, propertKey: string, descriptor: PropertyDescriptor) {
        const method = descriptor.value;
        descriptor.value = function(... args: any[]) {
            const t1 = performance.now();
            let returnValue;
            try {
                returnValue = method.apply(this, args);                 
                return returnValue;
            } finally {
                const t2 = performance.now();
                const executionTime = t2 - t1;
                if (executionTime > timeLimit) {
                    console.warn(`----------------------------------------------------------------`);
                    console.warn(`Result: ${propertKey}(${JSON.stringify(args)}) - Time: ${(t2 - t1).toFixed(5)}ms - Return: ${JSON.stringify(returnValue)}`);
                }
            }
            
        } 

        return descriptor;
    }
}
export enum eLogExecutionMode {
    Before = 0,
    BeforeAfer,
    After,
    None
}

export function logExecution(logExecutionMode: eLogExecutionMode = eLogExecutionMode.Before) {
    return function(target: any, propertKey: string, descriptor: PropertyDescriptor) {
        const method = descriptor.value;
        descriptor.value = function(... args: any[]) {
            try {
                if (logExecutionMode <= eLogExecutionMode.BeforeAfer) {
                    console.debug(`[BEFORE] ${propertKey}(${JSON.stringify(args)})`);
                }
                let returnValue = method.apply(this, args);                 
                return returnValue;
            } finally {
                if (logExecutionMode >= eLogExecutionMode.BeforeAfer && logExecutionMode <= eLogExecutionMode.After) {
                    console.debug(`[BEFORE] ${propertKey}(${JSON.stringify(args)})`);
                }
            }            
        } 

        return descriptor;
    }
}

export function classInit() {
    return function(constructor: any) {
        const original = constructor;

        const newConstructor: any = function(...args: any[]) {
            console.debug(`Creating instance of ${original.name}`);
            return new original(...args);
        } 

        newConstructor.prototype = original.prototype;
        return newConstructor;
    }
}