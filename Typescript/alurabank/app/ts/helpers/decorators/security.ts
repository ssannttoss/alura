// http://loopinfinito.com.br/2013/09/24/throttle-e-debounce-patterns-em-javascript/
export function throttle(milliseconds: number = 500) {
    return function(target: any, propertKey: string, descriptor: PropertyDescriptor) {
        const method = descriptor.value;
        let timer = 0;
        descriptor.value = function(... args: any[]) {
            if (event) {
                event.preventDefault();
            }

            // https://www.w3schools.com/jsref/met_win_clearinterval.asp
            clearInterval(timer);
            timer = setTimeout(() => method.apply(this, args), milliseconds);
        }           

        return descriptor;
    }
}
