import { logExecutionTime, logExecution }  from "../helpers/decorators/index";
export abstract class View<T> {
    private _element : JQuery;
    private _scape : boolean;
    
    constructor(selector: string, scape: boolean = true) {
        this._element = $(selector);
        this._scape = scape;
    }

    @logExecutionTime()
    @logExecution()
    update(model: T): void {
        let template = this.template(model);

        if (this._scape) {
            template = template.replace(/<script>[\s\S]*?<\/script>/,"");
        }

        this._element.html(template);
    }

    protected abstract template(model: T): string;
}