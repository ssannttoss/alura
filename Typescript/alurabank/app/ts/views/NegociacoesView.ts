import { View } from "./View";
import { Negociacoes } from "../models/Negociacoes";

export class NegociacoesView extends View<Negociacoes> {

    protected template(model: Negociacoes): string {
        return `
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <td>DATA</td>
                        <td>QUANTIDADE</td>
                        <td>VALOR</td>
                        <td>VOLUME</td>
                    </tr>
                </thead>

                <tbody>
                    ${model.paraArray().map(negociacao => {
                        return `
                            <tr>
                                <td>${negociacao.data.getDate()}/${negociacao.data.getMonth() + 1}/${negociacao.data.getFullYear()}</td>
                                <td>${negociacao.quantidade}</td>
                                <td>${negociacao.valor}</td>
                                <td>${negociacao.volume}</td>
                            </tr>
                        ` 
                    }).join("")}
                </tbody>

                <tfoot>
                </tfoot>
            </table>
            <script>alert(\"${this.constructor.name}\")</script>
        `;
    }
}