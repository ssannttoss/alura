import { logExecutionTime }  from "../helpers/decorators/monitors";
import { Entity } from "./Entity"
import { Printable } from "./Printable"
import { Negociacao } from "./Negociacao";

export class Negociacoes extends Entity implements Printable {
    // private _negociacoes: Array<Negociacao> = [];
    private _negociacoes: Negociacao[] = [];

    adicionar(negociacao: Negociacao): void {
        this._negociacoes.push(negociacao);
    }

    @logExecutionTime()
    paraArray(): Negociacao[] {
        return ([] as Negociacao[]).concat(this._negociacoes);
    }

    /**
     * https://cursos.alura.com.br/course/typescript-parte2/task/27956
     */
    funcaoDoisRetornos() : Negociacao | null {
        return null;
    }

    print(): void {
        console.log(JSON.stringify(this));
    }
}