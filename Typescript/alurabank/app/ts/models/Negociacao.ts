import { Entity } from "./Entity"
import { Printable } from "./Printable"
import { Equatable } from "./Equatable"

enum DiaSemana {
    Domingo = 0,
    Segunda,
    Terca,
    Quarta,
    Quinta,
    Sexta,
    Sabado
}

export class Negociacao extends Entity implements Printable, Equatable<Negociacao> {

    // private _data : Date;
    // private _quantidade : number;
    // private _valor : number;

    // constructor(data: Date, quantidade: number, valor: number) {
    //     this._data = data;
    //     this._quantidade = quantidade;
    //     this._valor = valor;
    // }

    constructor(readonly data: Date, readonly quantidade: number, readonly valor: number) {
        super();
    }

    get volume(): number {
        let vol = this.quantidade * this.valor;
        return vol; 
    }

    toString(): string {
        return JSON.stringify(this);
    }

    static ehDiaUtil(data: Date): Boolean {
        return data.getDay() != DiaSemana.Domingo && data.getDay() != DiaSemana.Sabado;
    }

    print(): void {
        console.log(JSON.stringify(this));
    }

    equals(negociacao: Negociacao): boolean {
        return this.data.getDate() == negociacao.data.getDate() &&
                this.data.getMonth() == negociacao.data.getMonth() &&
                this.data.getFullYear() == negociacao.data.getFullYear();
    }
}