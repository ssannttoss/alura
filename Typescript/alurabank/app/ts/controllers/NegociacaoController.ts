import { Negociacao, Negociacoes, NegociacaoApi } from "../models/index";
import { NegociacaoService, HandlerFunction } from "../services/index";
import { NegociacoesView, MensagemView } from "../views/index";
import { domInject, classInit, throttle } from "../helpers/decorators/index";
import { print } from "../helpers/index";

@classInit()
export class NegociacaoController {

    @domInject("#data")
    private _inputData : JQuery;
    @domInject("#quantidade")
    private _inputQuantidade: JQuery;
    @domInject("#valor")
    private _inputValor: JQuery;
    // private _negociacoes : Negociacoes = new Negociacoes(); // tipo declarado explicitament
    private _negociacoes = new Negociacoes(); // tipo declarado por inferência
    private _negociacoesView = new NegociacoesView("#negociacoes-view");
    private _mensagemView = new MensagemView("#mensagem-view");
    private _negociacaoService = new NegociacaoService();

    constructor() {
        this._negociacoesView.update(this._negociacoes);
    }

    @throttle(500)
    adicionar(e: Event): void {
        e.preventDefault();
        let data = new Date(this._inputData.val().replace(/-/g,","));

        if (!Negociacao.ehDiaUtil(data)) {
            this._mensagemView.update("Somente negociações em dias úteis.");
            return;
        }

        let negociacao = new Negociacao(
            data,
            parseInt(this._inputQuantidade.val()),
            parseFloat(this._inputValor.val())
        );

        this._negociacoes.adicionar(negociacao);
        this._negociacoesView.update(this._negociacoes);

        this._negociacoes.paraArray().forEach(negociacao => {
            console.info(negociacao.toString());
        });

        this._mensagemView.update("Negociação adicionada com sucesso");
        print(negociacao, this._negociacoes);
    }

    @throttle(500)
    importar() {
        /*
        // Function. Não é necessário importar a inteface que define a função
        // pois o método obterNegociacoes não irá aceitar caso a função não esteja em conformidade
        // com a interface.  
        function isOk(res: Response) {
            if (res.ok) {
                return res;
            }

            throw new Error(res.statusText);
        }
        */

        // Arrow Function tipada com a interface. Desta forma é necessário importar a interface
        // porém tem-se os benefícos de ter a validação na própria criação da função. 
        const isOk:HandlerFunction = (res: Response) => {
            if (res.ok) {
                return res;
            }

            throw new Error(res.statusText);
        }

        this._negociacaoService.obterNegociacoes(isOk)
            .then(negociacoes => {
                const negociacoesImportadas = this._negociacoes.paraArray();
                return negociacoes.filter(negociacaoNova => 
                    !negociacoesImportadas.some(negociacao => negociacao.equals(negociacaoNova)));
            })
            .then(negociacoes => {
                negociacoes.forEach(negociacao => this._negociacoes.adicionar(negociacao))
                this._negociacoesView.update(this._negociacoes);
                this._mensagemView.update("Negociações importadas com sucesso");
            });
    }
}

let instance = new NegociacaoController();
export function getInstance(): NegociacaoController { return instance; }