﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Repository
{
    public class EFContext : DbContext
    {
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<ItemPedido> ItensPedido { get; set; }

        public EFContext(DbContextOptions<EFContext> options) : base(options)
        {
        }
    }
}
