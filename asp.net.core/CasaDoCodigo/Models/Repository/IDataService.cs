﻿using Models.Entity;
using System.Collections.Generic;

namespace Models.Repository
{
    public interface IDataService
    {
        void Init();
        IList<Produto> GetProdutos();
        IList<ItemPedido> GetItensPedido();
    }
}