﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Repository
{
    public class ProdutoRepository
    {
        private IDataService dataService;
        public ProdutoRepository(IDataService dataService)
        {
            this.dataService = dataService;
        }

        public IList<Produto> obterListaProdutos()
        {
            return dataService.GetProdutos();
        }
    }
}
