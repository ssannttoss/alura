﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Models.Entity;

namespace Models.Repository
{
    public class DataService : IDataService
    {
        private readonly EFContext context;
        public DataService(EFContext context)
        {
            this.context = context;
        }

        public IList<ItemPedido> GetItensPedido()
        {
            return this.context.ItensPedido.ToList();
        }

        public IList<Produto> GetProdutos()
        {
            return this.context.Produtos.ToList();
        }

        public void Init()
        {
            context.Database.EnsureCreated();

            if (context.Produtos.Count() == 0)
            {
                var lst = new List<Produto>
                {
                    new Produto("Sleep not found", 59.90m),
                    new Produto("May the code be with you", 59.90m),
                    new Produto("Rollback", 59.90m),
                    new Produto("REST", 69.90m),
                    new Produto("Design Patterns com Java", 69.90m),
                    new Produto("Vire o jogo com Spring Framework", 69.90m),
                    new Produto("Test-Driven Development", 69.90m),
                    new Produto("iOS: Programe para iPhone e iPad", 69.90m),
                    new Produto("Desenvolvimento de Jogos para Android", 69.90m)
                };

                foreach(Produto produto in lst)
                {
                    context.Produtos.Add(produto);
                    context.ItensPedido.Add(new ItemPedido.Builder(produto).Quantidade(1).Build());
                }                
            }

            context.SaveChangesAsync();
        }
    }
}
