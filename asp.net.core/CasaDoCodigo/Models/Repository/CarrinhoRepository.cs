﻿using Models.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Models.Repository
{
    public class CarrinhoRepository
    {
        private IDataService dataService;
        public CarrinhoRepository(IDataService dataService)
        {
            this.dataService = dataService;
        }

        public IList<ItemPedido> obterListaPedidos(params Produto[] produtos)
        {
            return dataService.GetItensPedido();
        }        
    }
}
