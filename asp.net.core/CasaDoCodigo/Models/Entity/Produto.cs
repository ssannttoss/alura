﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models.Entity
{
    public class Produto
    {
        public int Id { get; private set; }
        public String Nome { get; private set; }
        public decimal Preco { get; private set; }

        private Produto()
        {

        }

        public Produto(String nome, decimal preco)
            :this()            
        {
            this.Nome = nome;
            this.Preco = preco;
        }

        public override string ToString()
        {
            return String.Format("{0} - {1} - R$ {2}", Id, Nome, Preco);
        }

        public class Builder
        {
            private Produto produto;
            
            public Builder()
            {
                this.produto = new Produto();
            }

            public Produto Build()
            {
                return this.produto;
            }

            public Builder Id(int id)
            {
                this.produto.Id = id;
                return this;
            }

            public Builder Nome(string nome)
            {
                this.produto.Nome = nome;
                return this;
            }

            public Builder Preco(decimal preco)
            {
                this.produto.Preco = preco;
                return this;
            }
        }
    }
}
