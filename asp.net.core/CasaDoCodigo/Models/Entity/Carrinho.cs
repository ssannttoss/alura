﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Models.Entity
{
    public class Carrinho
    {
        public IList<ItemPedido> ListaItemPedido { get; private set; }
        public decimal Total
        {
            get
            {
                return this.ListaItemPedido.Sum(p => p.SubTotal);
            }
        }

        public Carrinho(IList<ItemPedido> listaItemPedido)
        {
            this.ListaItemPedido = listaItemPedido;
        }
    }
}
