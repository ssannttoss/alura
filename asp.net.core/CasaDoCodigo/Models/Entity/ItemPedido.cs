﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models.Entity
{
    public class ItemPedido
    {
        public int Id { get; private set; }
        public Produto Produto { get; private set; }
        public int Quantidade { get; private set; }
        public decimal PrecoUnitario
        {
            get { return this.Produto.Preco; }
        }
        public decimal SubTotal
        {
            get { return this.Produto.Preco * this.Quantidade; }
        }

        private ItemPedido() { }

        public ItemPedido(int id, Produto produto, int quantidade)
            : this()
        {
            this.Id = id;
            this.Produto = produto;
            this.Quantidade = quantidade;
        }

        public class Builder
        {
            private ItemPedido itemPedido;

            public Builder(Produto produto)
            {
                this.itemPedido = new ItemPedido();
                this.itemPedido.Produto = produto;
            }

            public ItemPedido Build()
            {
                return this.itemPedido;
            }

            public Builder Id(int id)
            {
                this.itemPedido.Id = id;
                return this;
            }

            public Builder Quantidade(int id)
            {
                this.itemPedido.Quantidade = id;
                return this;
            }
        }
    }
}
