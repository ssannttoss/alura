﻿using Microsoft.AspNetCore.Mvc;
using Models.Entity;
using Models.Repository;
using System.Collections.Generic;

namespace CasaDoCodigo.Controllers
{
    public class PedidoController : Controller
    {
        private IDataService dataService;
        public PedidoController(IDataService dataService)
        {
            this.dataService = dataService;
        }

        public IActionResult Carrossel()
        {
            IList<Produto> listaProdutos = new ProdutoRepository(dataService).obterListaProdutos();
            return View(listaProdutos);
        }

        public IActionResult Carrinho()
        {
            IList<Produto> listaProdutos = new ProdutoRepository(dataService).obterListaProdutos();
            IList<ItemPedido> listaItemPedido = new CarrinhoRepository(dataService).obterListaPedidos(listaProdutos[0], listaProdutos[1]);
            Carrinho carrinho = new Carrinho(listaItemPedido);
            return View(carrinho);
        }

        public IActionResult Resumo()
        {
            return View();
        }
    }
}