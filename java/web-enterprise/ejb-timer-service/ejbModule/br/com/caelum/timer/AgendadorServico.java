package br.com.caelum.timer;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 * Session Bean implementation class AgendadorServico
 */
@Singleton
@Startup
public class AgendadorServico {

	@Schedule(hour="*", minute="*", second="*/10") //each 10 seconds
    void agendado() {
    	System.out.println("verificando serviço externo periodicamente");
    }

}
