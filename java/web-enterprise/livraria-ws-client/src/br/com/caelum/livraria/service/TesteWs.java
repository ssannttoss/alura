package br.com.caelum.livraria.service;

import java.rmi.RemoteException;
import java.util.Arrays;

public class TesteWs {
	public static void main(String[] args) throws RemoteException {
		LivrariaWSProxy livrariaWSProxy = new LivrariaWSProxy();
		Livro[] livrosPorTitulo = livrariaWSProxy.getLivrosPorTitulo("Arquitetura");
		Arrays.stream(livrosPorTitulo).forEach(System.out::println);
	}
}
