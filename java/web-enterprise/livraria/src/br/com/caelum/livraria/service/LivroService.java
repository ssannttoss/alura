package br.com.caelum.livraria.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.caelum.livraria.dao.LivroDao;
import br.com.caelum.livraria.modelo.Livro;

@Stateless
public class LivroService {

	@Inject
	LivroDao livroDao;
	
	public void salva(Livro livro) {
		livroDao.salva(livro);
	}
	
	public List<Livro> todosLivros() {
		return livroDao.todosLivros();
	}

	public List<Livro> getLivrosPorTitulo(String titulo) {
		List<Livro> list = livroDao.todosLivros().stream()
			.filter((livro) -> livro.getTitulo().contains(titulo))
			.collect(Collectors.toList());
		
		return list;
	}
}
