package br.com.caelum.livraria.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import br.com.caelum.livraria.dao.AutorDao;
import br.com.caelum.livraria.interceptador.LogInterceptador;
import br.com.caelum.livraria.modelo.Autor;

@Stateless
//@TransactionManagement(TransactionManagementType.CONTAINER) //default, option
@Interceptors({LogInterceptador.class})
public class AutorService {
	
	@Inject
	AutorDao autorDao;
	
	//@TransactionAttribute(TransactionAttributeType.REQUIRED) //default, optional
	public void salva(Autor autor) {
		autorDao.salva(autor);
	}

	//@TransactionAttribute(TransactionAttributeType.REQUIRED) //default, optional
	public List<Autor> todosAutores() {
		return autorDao.todosAutores();
	}
	
	//@TransactionAttribute(TransactionAttributeType.REQUIRED) //default, optional
	public Autor buscaPelaId(Integer autorId) {
		Autor autor = autorDao.buscaPelaId(autorId);
		return autor;
	}
	
}
