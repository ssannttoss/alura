package br.com.caelum.livraria.interceptador;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class LogInterceptador {
	@AroundInvoke
	public Object log(InvocationContext context) throws Exception {
		final String className = context.getTarget().getClass().getSimpleName();
		final String methodName = context.getMethod().getName();
		final long millis = System.currentTimeMillis();
		final Object proceed = context.proceed();		
		System.out.println("[info] " + className + "." + methodName + " - Tempo gasto na execução: " + (System.currentTimeMillis() - millis));
		return proceed;
	}
}
