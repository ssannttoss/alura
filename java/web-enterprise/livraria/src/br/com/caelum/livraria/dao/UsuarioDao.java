package br.com.caelum.livraria.dao;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.caelum.livraria.modelo.Usuario;

@Stateless
public class UsuarioDao {

	@PersistenceContext
	private EntityManager banco;
	
	@PostConstruct
	void postConstruct() {
		System.out.println("[info] AutorDAO foi criado.");
	}

	public Usuario buscaPeloLogin(String login) {
		// JPQL
		final Query query = banco.createQuery("select u from Usuario u where u.login=:pLogin");
		query.setParameter("pLogin", login);
		return (Usuario) query.getSingleResult();
	}
	
}
