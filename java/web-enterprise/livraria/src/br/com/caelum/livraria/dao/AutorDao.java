package br.com.caelum.livraria.dao;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.caelum.livraria.modelo.Autor;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER) //default, optional
public class AutorDao {

	@PersistenceContext
	private EntityManager banco;
	
	@PostConstruct
	void postConstruct() {
		System.out.println("[info] AutorDAO foi criado.");
	}

	//@TransactionAttribute(TransactionAttributeType.REQUIRED) //default, optional
	@TransactionAttribute(TransactionAttributeType.MANDATORY) //a transaction must be opened before
	public void salva(Autor autor) {
		banco.persist(autor);
	}
	
	public List<Autor> todosAutores() {
		final TypedQuery<Autor> createQuery = banco.createQuery("select a from Autor a", Autor.class);
		return createQuery.getResultList();
	}

	public Autor buscaPelaId(Integer autorId) {
		Autor autor = banco.find(Autor.class, autorId);
		return autor;
	}
	
}
