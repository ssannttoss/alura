package br.com.caelum.livraria.dao;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.caelum.livraria.modelo.Livro;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER) //default, optional
public class LivroDao {

	@PersistenceContext
	private EntityManager banco;
	
	@PostConstruct
	void postConstruct() {
		System.out.println("[info] AutorDAO foi criado.");
	}
	
	@TransactionAttribute(TransactionAttributeType.MANDATORY) //a transaction must be opened before
	public void salva(Livro livro) {
		banco.persist(livro);
	}
	
	public List<Livro> todosLivros() {
		final TypedQuery<Livro> createQuery = banco.createQuery("select l from Livro l", Livro.class);
		return createQuery.getResultList();
	}
	
}
