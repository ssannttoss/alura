package br.com.caelum.estoque.ws;

import java.io.IOException;

import javax.xml.ws.Endpoint;

public class PublicEstoqueWS {
	public static void main(String[] args) throws IOException {
		final EstoqueWS estoqueWS = new EstoqueWS();
		final String url = "http://localhost:8181/estoquews";
		System.out.println("Estoque rodando: " + url);
		Endpoint.publish(url,  estoqueWS);
		System.in.read();
		System.out.println("bye");
	}
}
