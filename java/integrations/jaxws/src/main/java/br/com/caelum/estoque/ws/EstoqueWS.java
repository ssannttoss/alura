package br.com.caelum.estoque.ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import br.com.caelum.estoque.modelo.excecoes.AutorizacaoException;
import br.com.caelum.estoque.modelo.item.Filtro;
import br.com.caelum.estoque.modelo.item.Filtros;
import br.com.caelum.estoque.modelo.item.Item;
import br.com.caelum.estoque.modelo.item.ItemDao;
import br.com.caelum.estoque.modelo.usuario.TokenDao;
import br.com.caelum.estoque.modelo.usuario.TokenUsuario;

@WebService
public class EstoqueWS {
	final private ItemDao itemDao;

	public EstoqueWS() {
		itemDao = new ItemDao();		
	}

	@WebMethod(operationName="TodosOsItens")
	@RequestWrapper(localName="listaItens")
	@ResponseWrapper(localName="itens")
	@WebResult(name="itens")
	public List<Item> getItens(@WebParam(name="filtros") Filtros filtros) {
		System.out.println("Chamando todosItens()"); 
		final List<Filtro> lista = filtros.getLista();
		final List<Item> itensResultado = itemDao.todosItens(lista);
		//return new ListaItens(itensResultado);
		return itensResultado; // with @ResponseWrapper we do not need wrapper the result in custom list class
	}
	
	@WebMethod(operationName="CadastrarItem")
	@WebResult(name="item")
	public Item cadastrarItem(
			@WebParam(name="tokenUsuario", header=true) TokenUsuario tokenUsuario,
			@WebParam(name="item") Item item) 
					throws AutorizacaoException {
		System.out.println("Cadastrando item " + item + ", Token: " + tokenUsuario);
		final boolean valido = new TokenDao().ehValido(tokenUsuario);
		
		if (!valido) {
			throw new AutorizacaoException("Authorizacao falhou");
		}
		
		this.itemDao.cadastrar(item);
		return item;
	}
	
}
