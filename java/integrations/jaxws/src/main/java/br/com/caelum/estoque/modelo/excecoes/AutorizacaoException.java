package br.com.caelum.estoque.modelo.excecoes;

import java.util.Date;

import javax.xml.ws.WebFault;

@WebFault
public class AutorizacaoException extends Exception {

	public AutorizacaoException(String mensagem) {
		super(mensagem);
	}

	public InfoFault getFaultInfo() {
		return new InfoFault(new Date(), "Token Inválido");
	}
}
