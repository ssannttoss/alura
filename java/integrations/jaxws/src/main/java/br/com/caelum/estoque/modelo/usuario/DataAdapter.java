package br.com.caelum.estoque.modelo.usuario;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DataAdapter extends XmlAdapter<String, Date> {
	private static final String PATTERN = "dd/MM/yyyy";

	@Override
	public Date unmarshal(String str) throws Exception {
		System.out.println(str);
		return new SimpleDateFormat(PATTERN).parse(str);
	}

	@Override
	public String marshal(Date date) throws Exception {
		System.out.println(date);
		return new SimpleDateFormat(PATTERN).format(date);
	}

}
