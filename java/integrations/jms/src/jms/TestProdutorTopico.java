package jms;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class TestProdutorTopico {
	private static final boolean USES_TRANSACTION = false;

	public static void main(String[] args)  throws NamingException, JMSException, InterruptedException {
		final InitialContext ctx = new InitialContext();
		final ConnectionFactory factory = (ConnectionFactory)ctx.lookup("ConnectionFactory");
		final Connection connection = factory.createConnection();
		connection.start();
		final Session session = connection.createSession(USES_TRANSACTION, Session.AUTO_ACKNOWLEDGE);
		final Destination topico = (Destination)ctx.lookup("loja");
		final MessageProducer producer = session.createProducer(topico);
		final TextMessage message = session.createTextMessage("tópico: " + new Date());
		Calendar c = Calendar.getInstance();
		
		for(int i = 0; i < 10; i++) {
			c.setTime(new Date());
			message.setBooleanProperty("ebook", c.get(Calendar.SECOND) % 2 == 0);
			producer.send(message);
			System.out.println("Sent: " + message);
			Thread.sleep(new Random().nextInt(1002));
		}
		
		producer.close();
		session.close();
		connection.close();
		ctx.close();
		
	}

}
