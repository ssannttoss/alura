package jms;

import java.util.Scanner;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class QueueTest {
	private static final String QUEUE_NAME_FINANCEIRO = "financeiro";
	private static final String ACTIVE_MQ_CONN_FACTORY = "ConnectionFactory";

	@SuppressWarnings("resource")
	public static void main(String[] args) throws NamingException, JMSException {
		final InitialContext ctx = new InitialContext();
		final QueueConnectionFactory cf = (QueueConnectionFactory)ctx.lookup(ACTIVE_MQ_CONN_FACTORY);
		final QueueConnection connection = cf.createQueueConnection();
		connection.start();
		
		final QueueSession session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
		final Queue queue = (Queue)ctx.lookup(QUEUE_NAME_FINANCEIRO);
		final QueueReceiver receiver = session.createReceiver(queue);
		
		final Message message = receiver.receive();
		System.out.println(message);
		new Scanner(System.in).nextLine();
		receiver.close();
		session.close();
		connection.close();
		ctx.close();		
		
	}
}
