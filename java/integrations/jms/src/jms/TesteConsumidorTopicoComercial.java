package jms;

import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class TesteConsumidorTopicoComercial {
	private static final boolean USES_TRANSACTION = false;
	
	public static void main(String[] args) throws NamingException, JMSException {
		final InitialContext ctx = new  InitialContext(); //loads from jndi.properties file
//		final InitialContext ctx = new  InitialContext(getProperties()); //loads from memory properties
		final ConnectionFactory cf = (ConnectionFactory) ctx.lookup("ConnectionFactory");
		final Connection jmsConn = cf.createConnection();
		jmsConn.setClientID("comercial");
		jmsConn.start();
		
		// adding a consumer
		final Session session = jmsConn.createSession(USES_TRANSACTION, Session.AUTO_ACKNOWLEDGE);
		final Topic topic = (Topic)ctx.lookup("loja");
		final MessageConsumer consumer = session.createDurableSubscriber(topic, "assinatura");		
		
		//final Message message = consumer.receive();
		consumer.setMessageListener(new MessageListener() {
			@Override
			public void onMessage(Message message) {
				TextMessage textMessage = (TextMessage)message;
				try {
					if (textMessage != null) {
						System.out.println(textMessage.getText());
					}
				} catch (JMSException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		System.out.println("Consumidor tópicos " + jmsConn.getClientID());
		
		try (Scanner scanner = new Scanner(System.in)) {
			final String line = scanner.nextLine();	
			System.out.println(line);
		}
		
		consumer.close();
		session.close();
			
		jmsConn.close();
		ctx.close();
	}

}
