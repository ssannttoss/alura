package jms;

import java.util.Enumeration;
import java.util.Properties;
import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class TesteConsumidor {
	private static final boolean USES_TRANSACTION = false;
	
	private static Properties getProperties() {
		final Properties properties = new Properties();
		properties.setProperty("java.naming.factory.initial", "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
		properties.setProperty("java.naming.provider.url", "tcp://localhost:61616");
		properties.setProperty("queue.financeiro", "fila.financeiro");
		return properties;
	}

	public static void main(String[] args) throws NamingException, JMSException {
		//final InitialContext ctx = new  InitialContext(); loads from jndi.properties file
		final InitialContext ctx = new  InitialContext(getProperties()); //loads from memory properties
		final ConnectionFactory cf = (ConnectionFactory) ctx.lookup("ConnectionFactory");
		final Connection jmsConn = cf.createConnection();
		jmsConn.setClientID("financeiro");
		jmsConn.start();
		
		// adding a consumer
		final Session session = jmsConn.createSession(USES_TRANSACTION, Session.AUTO_ACKNOWLEDGE);
		final Destination queue = (Destination)ctx.lookup("financeiro");
		final MessageConsumer consumer = session.createConsumer(queue);
		final QueueBrowser browser = session.createBrowser((Queue) queue);
		
		 // Browses messages without consuming it form queue
		final Enumeration<Message> enumeration = browser.getEnumeration();
		while (enumeration.hasMoreElements()) {
			TextMessage textMessage = (TextMessage) enumeration.nextElement();
			System.out.println(textMessage.getText());
		}
		
		//final Message message = consumer.receive();
		consumer.setMessageListener(new MessageListener() {
			@Override
			public void onMessage(Message message) {
				TextMessage textMessage = (TextMessage)message;
				try {
					if (textMessage != null) {
						System.out.println(textMessage.getText());
					}
				} catch (JMSException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		System.out.println("Consumidor fila " + jmsConn.getClientID());
		
		try (Scanner scanner = new Scanner(System.in)) {
			final String line = scanner.nextLine();	
			System.out.println(line);
		}
		
		consumer.close();
		session.close();
			
		jmsConn.close();
		ctx.close();
	}
}
