package jms;

import java.util.Date;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class TesteProdutor {
	private static final boolean USES_TRANSACTION = false;
	
	public static void main(String[] args) throws NamingException, JMSException {
		final InitialContext ctx = new InitialContext();
		final ConnectionFactory factory = (ConnectionFactory)ctx.lookup("ConnectionFactory");
		final Connection connection = factory.createConnection();
		connection.start();
		final Session session = connection.createSession(USES_TRANSACTION, Session.AUTO_ACKNOWLEDGE);
		final Destination queue = (Destination)ctx.lookup("financeiro");
		final MessageProducer producer = session.createProducer(queue);
		final TextMessage message = session.createTextMessage("fila: " + new Date());
		producer.send(message);
		producer.close();
		session.close();
		connection.close();
		ctx.close();
		
	}
}
