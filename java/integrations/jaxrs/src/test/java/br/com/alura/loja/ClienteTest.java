package br.com.alura.loja;

import static org.junit.Assert.assertTrue;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;

import org.junit.Test;

public class ClienteTest {

	private static final String MOCKY_RESOURCE = "/v2/52aaf5deee7ba8c70329fb7d";
	private static final String MOCKY_BASE_URL = "http://www.mocky.io";

	@Test
	public void testa_que_a_conexao_servico_funciona() {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(MOCKY_BASE_URL);
		WebTarget path = webTarget.path(MOCKY_RESOURCE);
		Builder request = path.request();
		String response = request.get(String.class);		
		assertTrue(response.contains("Rua Vergueiro 3185, 8 andar"));
		
	}	
}
