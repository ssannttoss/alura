package br.com.alura.loja;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.thoughtworks.xstream.XStream;

import br.com.alura.dto.ResponseDto;
import br.com.alura.loja.modelo.Carrinho;
import br.com.alura.loja.modelo.Produto;

public class CarrinhoTest {
	private static final String BASE_URL = "http://localhost:8081";
	private static final String CARRINHO_RESOURCE = "/carrinhos";

	private HttpServer httpServer;
	private Client client;

	@Before
	public void before() {
		URI uri = URI.create(BASE_URL);
		ResourceConfig resourceConfig = new ResourceConfig();
		ResourceConfig packages = resourceConfig.packages("br.com.alura.loja.resource");
		httpServer = GrizzlyHttpServerFactory.createHttpServer(uri, packages);
		final ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(new LoggingFilter());
		client = ClientBuilder.newClient(clientConfig);
	}
	
	@After
	public void after() {
		httpServer.stop();
	}

	private Carrinho obterCarrinho(long id) {
		
		final WebTarget webTarget = client.target(BASE_URL);
		final WebTarget path = webTarget.path(CARRINHO_RESOURCE + "/" + id);
		final Builder request = path.request();
		
		/* 
		Using XStream
		final String response = request.get(String.class);	
		final Carrinho carrinho = (Carrinho)new XStream().fromXML(response);
		*/
		final Carrinho carrinho = request.get(Carrinho.class);
		return carrinho;
	}
	
	@Test
	public void obter_carrinho_pelo_id_test() {
		final Carrinho carrinho = obterCarrinho(1); 
		assertTrue(carrinho.getRua().contains("Rua Vergueiro 3185, 8 andar"));
	}
	

	@Test
	public void adicionar_carrinho_test() {
		final Carrinho carrinho = new Carrinho();
		carrinho.adiciona(new Produto(314L, "Tablet", 999, 1));
		carrinho.setCidade("Nova Lima");
		carrinho.setRua("Cruzeiro, 99");		

		final Client client = ClientBuilder.newClient();
		final WebTarget webTarget = client.target(BASE_URL);
		final WebTarget path = webTarget.path(CARRINHO_RESOURCE);
		final Builder request = path.request();
		
		/*
		XStream
		final String xml = carrinho.toXML();
		final Entity<String> entity = Entity.entity(xml, MediaType.APPLICATION_XML);
		*/
		final Entity<Carrinho> entity = Entity.entity(carrinho, MediaType.APPLICATION_XML);
		
		final Response response = request.post(entity);	
		int status = response.getStatus();
		assertEquals(Response.Status.CREATED.getStatusCode(), status);
		final String responseBody = response.readEntity(String.class);	
		final ResponseDto dto = (ResponseDto)new XStream().fromXML(responseBody); 
		assertTrue(dto.getStatus().contains("sucesso"));
		
		final Carrinho carrinhoAdicionado = obterCarrinho(dto.getId());
		assertEquals(carrinhoAdicionado.getId(), dto.getId());
	}
	
	@Test
	public void remover_carrinho_test() {
		final Carrinho carrinho = obterCarrinho(1);
		final Client client = ClientBuilder.newClient();
		final WebTarget webTarget = client.target(BASE_URL);
		final WebTarget path = webTarget.path(CARRINHO_RESOURCE + "/" + carrinho.getId() + "/produtos/" + carrinho.getProdutos().get(0).getId());
		final Builder request = path.request();
		final Response response = request.delete();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());		
	}

}
