package br.com.alura.loja;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.thoughtworks.xstream.XStream;

import br.com.alura.dto.ResponseDto;
import br.com.alura.loja.modelo.Projeto;

public class ProjetoTest {
	private static final String BASE_URL = "http://localhost:8082";
	private static final String PROJETO_RESOURCE = "/projetos";

	private HttpServer httpServer;

	@Before
	public void before() {
		URI uri = URI.create(BASE_URL);
		ResourceConfig resourceConfig = new ResourceConfig();
		ResourceConfig packages = resourceConfig.packages("br.com.alura.loja.resource");
		httpServer = GrizzlyHttpServerFactory.createHttpServer(uri, packages);		
	}
	
	@After
	public void after() {
		httpServer.stop();
	}
	
	@Test
	public void adicionar_projeto_test() {
		final Projeto projeto = new Projeto("FITec", 2018);				

		final Client clientPost = ClientBuilder.newClient();
		final WebTarget webTargetPost = clientPost.target(BASE_URL);
		final WebTarget pathPost = webTargetPost.path(PROJETO_RESOURCE);
		final Builder requestPost = pathPost.request();
		
		/*
		XStream
		final String xml = projeto.toXML();
		final Entity<String> entity = Entity.entity(xml, MediaType.APPLICATION_XML);
		*/
		final Entity<Projeto> entity = Entity.entity(projeto, MediaType.APPLICATION_XML);
		
		final Response responsePost = requestPost.post(entity);	
		final String responseBodyPost = responsePost.readEntity(String.class);
		final ResponseDto dto = (ResponseDto)new XStream().fromXML(responseBodyPost);
		
		assertTrue(dto.getStatus().contains("sucesso"));
		
		final Client clientGet = ClientBuilder.newClient();
		final WebTarget webTarget = clientGet.target(BASE_URL);
		final WebTarget pathGet = webTarget.path(PROJETO_RESOURCE + "/" + dto.getId());
		final Builder requestGet = pathGet.request();
		
		/*final String responseGet = requestGet.get(String.class);	
		final Projeto projetoGet = (Projeto)new XStream().fromXML(responseGet);*/
		final Projeto projetoGet = requestGet.get(Projeto.class);
		assertTrue(projetoGet.getNome().equals("FITec"));		
	}
	
	@Test
	public void deletar_projeto() {
		adicionar_projeto_test();
		final Client client = ClientBuilder.newClient();
		final WebTarget path = client.target(BASE_URL).path(PROJETO_RESOURCE + "/" + 1l);
		final Builder request = path.request();
		final Response response = request.delete();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
	}

}
