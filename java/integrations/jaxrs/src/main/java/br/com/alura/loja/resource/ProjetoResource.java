package br.com.alura.loja.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.thoughtworks.xstream.XStream;

import br.com.alura.dto.ResponseDto;
import br.com.alura.loja.dao.ProjetoDAO;
import br.com.alura.loja.modelo.Projeto;

@Path("projetos")
public class ProjetoResource {

	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Projeto busca(@PathParam("id")long id) {
		final Projeto projeto = new ProjetoDAO().busca(id);
		//return projeto.toXML(); XStream
		return projeto;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_XML)
	public String adicionar(Projeto projeto) {
		//final Projeto projeto = (Projeto)new  XStream().fromXML(content); XStream
		final long id = new ProjetoDAO().adiciona(projeto);
		final String xml = new XStream().toXML(new ResponseDto("sucesso", id));
		return xml;
	}
	
	@Path("{id}")
	@DELETE	
	public Response deletar(@PathParam("id")long id) {
		final boolean remove = new ProjetoDAO().remove(id);
		if (remove) {
			return Response.ok().build();
		} else {
			return Response.status(Status.NOT_FOUND).build();
		}
	}
}
