package br.com.alura.loja.resource;

import java.net.URI;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.thoughtworks.xstream.XStream;

import br.com.alura.dto.ResponseDto;
import br.com.alura.loja.dao.CarrinhoDAO;
import br.com.alura.loja.modelo.Carrinho;

@Path("carrinhos")
public class CarrinhoResource {
	
	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Carrinho buscar(@PathParam("id")long id) {
		Carrinho carrinho = new CarrinhoDAO().busca(id);
		//return carrinho.toXML(); XStrem
		return carrinho; //JAXB
	}
	
	@Path("{id}/json")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String buscarJson(@PathParam("id")long id) {
		Carrinho carrinho = new CarrinhoDAO().busca(id);
		return carrinho.toJSON();
	}
	
	@POST
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_XML)
	public Response adicionar(Carrinho carrinho) {
		//final Carrinho carrinho = (Carrinho)new  XStream().fromXML(content); //XStrem		
		final long id = new CarrinhoDAO().adiciona(carrinho);
		final URI uri = URI.create("/carrinhos/" + id);
		final ResponseBuilder created = Response.created(uri);
		created.entity(new ResponseDto("sucesso", id).toXML());
		try {
			final Response response = created.build();
			return response;
		} catch(Exception e) {
			System.out.println(e);
		}
		
		return Response.serverError().build();
	}
	
	@Path("{id}/produtos/{produtoId}")
	@DELETE
	public Response removerProduto(@PathParam("id")long id, @PathParam("produtoId")long produtoId) {
		final Carrinho carrinho = new CarrinhoDAO().busca(id);
		carrinho.remove(produtoId);
		final ResponseBuilder ok = Response.ok();
		return ok.build();
	}
	
}
