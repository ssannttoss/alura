package br.com.alura.loja.modelo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.thoughtworks.xstream.XStream;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Projeto {
	private long id;
	private String nome;
	private int anoInicio;
	
	public Projeto() {
	}
	
	public Projeto(String nome, int anoInicio) {
		this();
		this.nome = nome;
		this.anoInicio = anoInicio;
	}
	
	public Projeto(long id, String nome, int anoInicio) {		
		this(nome, anoInicio);
		this.id = id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public int getAnoInicio() {
		return anoInicio;
	}	
	
	public String toXML() {
		return new XStream().toXML(this);
	}
}
