package br.com.alura.loja;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

public class Servidor {
	private static final String SERVER_URL = "htt://localhost:8080/";

	public static void main(String[] args) throws IOException {
		URI uri = URI.create(SERVER_URL);
		ResourceConfig resourceConfig = new ResourceConfig();
		ResourceConfig packages = resourceConfig.packages("br.com.alura.loja.resource");
		HttpServer httpServer = GrizzlyHttpServerFactory.createHttpServer(uri, packages);
		System.out.println("Servdor rodando");
		System.in.read();
		httpServer.stop();
	}
}
