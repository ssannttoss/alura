package br.com.alura.dto;

import com.thoughtworks.xstream.XStream;

public class ResponseDto {
	private final String status;	
	private final long id;
	
	public ResponseDto(String status, long id) {
		this.status = status;
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public long getId() {
		return id;
	}
	
	public String toXML() {
		return new XStream().toXML(this);
	}
}
