/**
 * EstoqueWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.caelum.estoque.ws;

public interface EstoqueWS extends java.rmi.Remote {
    public br.com.caelum.estoque.ws.Item[] todosOsItens(br.com.caelum.estoque.ws.ListaItens parameters) throws java.rmi.RemoteException;
    public br.com.caelum.estoque.ws.CadastrarItemResponse cadastrarItem(br.com.caelum.estoque.ws.CadastrarItem parameters, br.com.caelum.estoque.ws.TokenUsuario tokenUsuario) throws java.rmi.RemoteException, br.com.caelum.estoque.ws.InfoFault;
}
