package br.com.caelum.estoque.ws;

public class EstoqueWSProxy implements br.com.caelum.estoque.ws.EstoqueWS {
  private String _endpoint = null;
  private br.com.caelum.estoque.ws.EstoqueWS estoqueWS = null;
  
  public EstoqueWSProxy() {
    _initEstoqueWSProxy();
  }
  
  public EstoqueWSProxy(String endpoint) {
    _endpoint = endpoint;
    _initEstoqueWSProxy();
  }
  
  private void _initEstoqueWSProxy() {
    try {
      estoqueWS = (new br.com.caelum.estoque.ws.EstoqueWSServiceLocator()).getEstoqueWSPort();
      if (estoqueWS != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)estoqueWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)estoqueWS)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (estoqueWS != null)
      ((javax.xml.rpc.Stub)estoqueWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.com.caelum.estoque.ws.EstoqueWS getEstoqueWS() {
    if (estoqueWS == null)
      _initEstoqueWSProxy();
    return estoqueWS;
  }
  
  public br.com.caelum.estoque.ws.Item[] todosOsItens(br.com.caelum.estoque.ws.ListaItens parameters) throws java.rmi.RemoteException{
    if (estoqueWS == null)
      _initEstoqueWSProxy();
    return estoqueWS.todosOsItens(parameters);
  }
  
  public br.com.caelum.estoque.ws.CadastrarItemResponse cadastrarItem(br.com.caelum.estoque.ws.CadastrarItem parameters, br.com.caelum.estoque.ws.TokenUsuario tokenUsuario) throws java.rmi.RemoteException, br.com.caelum.estoque.ws.InfoFault{
    if (estoqueWS == null)
      _initEstoqueWSProxy();
    return estoqueWS.cadastrarItem(parameters, tokenUsuario);
  }
  
  
}