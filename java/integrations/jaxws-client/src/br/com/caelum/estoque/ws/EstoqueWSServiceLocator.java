/**
 * EstoqueWSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.caelum.estoque.ws;

public class EstoqueWSServiceLocator extends org.apache.axis.client.Service implements br.com.caelum.estoque.ws.EstoqueWSService {

    public EstoqueWSServiceLocator() {
    }


    public EstoqueWSServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public EstoqueWSServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for EstoqueWSPort
    private java.lang.String EstoqueWSPort_address = "http://localhost:8080/jaxws/EstoqueWS";

    public java.lang.String getEstoqueWSPortAddress() {
        return EstoqueWSPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String EstoqueWSPortWSDDServiceName = "EstoqueWSPort";

    public java.lang.String getEstoqueWSPortWSDDServiceName() {
        return EstoqueWSPortWSDDServiceName;
    }

    public void setEstoqueWSPortWSDDServiceName(java.lang.String name) {
        EstoqueWSPortWSDDServiceName = name;
    }

    public br.com.caelum.estoque.ws.EstoqueWS getEstoqueWSPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(EstoqueWSPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getEstoqueWSPort(endpoint);
    }

    public br.com.caelum.estoque.ws.EstoqueWS getEstoqueWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.com.caelum.estoque.ws.EstoqueWSServiceSoapBindingStub _stub = new br.com.caelum.estoque.ws.EstoqueWSServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getEstoqueWSPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setEstoqueWSPortEndpointAddress(java.lang.String address) {
        EstoqueWSPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.com.caelum.estoque.ws.EstoqueWS.class.isAssignableFrom(serviceEndpointInterface)) {
                br.com.caelum.estoque.ws.EstoqueWSServiceSoapBindingStub _stub = new br.com.caelum.estoque.ws.EstoqueWSServiceSoapBindingStub(new java.net.URL(EstoqueWSPort_address), this);
                _stub.setPortName(getEstoqueWSPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("EstoqueWSPort".equals(inputPortName)) {
            return getEstoqueWSPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws.estoque.caelum.com.br/", "EstoqueWSService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws.estoque.caelum.com.br/", "EstoqueWSPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("EstoqueWSPort".equals(portName)) {
            setEstoqueWSPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
