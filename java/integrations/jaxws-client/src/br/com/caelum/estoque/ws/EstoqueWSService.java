/**
 * EstoqueWSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.caelum.estoque.ws;

public interface EstoqueWSService extends javax.xml.rpc.Service {
    public java.lang.String getEstoqueWSPortAddress();

    public br.com.caelum.estoque.ws.EstoqueWS getEstoqueWSPort() throws javax.xml.rpc.ServiceException;

    public br.com.caelum.estoque.ws.EstoqueWS getEstoqueWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
