package com.ssannttoss.alura.java.tdd.math;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {

	private Calculator calculator;

	@Before
	public void setup() {
		calculator = new MyCalculator();
	}

	@Test
	public void multiply_a_by_0_should_return_0() {
		final double a = 5;
		assertThat(calculator.multiply(a, 0), equalTo(a * 0));
	}

	@Test
	public void multiply_a_by_a_should_return_a_multiplied_by_a() {
		final double a = 2.3;
		assertThat(calculator.multiply(a, a), equalTo(a * a));
	}

	@Test
	public void multiply_a_by_b_should_return_a_multiplied_by_b() {
		final double a = 1;
		final double b = 2;
		assertThat(calculator.multiply(a, b), equalTo(a * b));
	}

	@Test
	public void substract_of_a_from_0_should_return_a() {
		final double a = 5.5;
		assertThat(calculator.subtract(a, 0), equalTo(a - 0));
	}

	@Test
	public void subtract_b_from_a_should_return_a_minus_b() {
		final double a = 1;
		final double b = 2;
		assertThat(calculator.subtract(a, b), equalTo(a - b));
	}

	@Test
	public void subtract_of_a_from_a_should_return_0() {
		final double a = 7.34;
		assertThat(calculator.subtract(a, a), equalTo(a - a));
	}

	@Test
	public void sum_of_a_and_0_should_return_a() {
		final double a = 5;
		assertThat(calculator.sum(a, 0), equalTo(a + 0));
	}

	@Test
	public void sum_of_a_and_a_should_return_a_plus_a() {
		final double a = 2.3;
		assertThat(calculator.sum(a, a), equalTo(a + a));
	}

	@Test
	public void sum_of_a_and_b_should_return_a_plus_b() {
		final double a = 1;
		final double b = 2;
		assertThat(calculator.sum(a, b), equalTo(a + b));
	}

	@Test(expected = DivideByZeroException.class)
	public void divide_a_by_0_should_throw_divide_by_zero_exception() throws DivideByZeroException {
		final double a = 5;
		assertThat(calculator.divide(a, 0), equalTo(0)); // not expected
	}

	@Test
	public void divide_a_by_a_when_a_not_zero_should_return_1() throws DivideByZeroException {
		final double a = 2.3;
		assertThat(calculator.divide(a, a), equalTo(a / a));
	}

	@Test
	public void divide_a_by_b_when_0_not_zero_should_return_a_divided_by_b() throws DivideByZeroException {
		final double a = 1;
		final double b = 2;
		assertThat(calculator.divide(a, b), equalTo(a / b));
	}
}
