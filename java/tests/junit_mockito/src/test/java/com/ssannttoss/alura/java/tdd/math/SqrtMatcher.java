/**
 *
 */
package com.ssannttoss.alura.java.tdd.math;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.TypeSafeMatcher;

/**
 * @author ssannttoss
 *
 */
public class SqrtMatcher extends TypeSafeMatcher<Double> {

	private final double a;

	public SqrtMatcher(final double a) {
		this.a = a;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.hamcrest.TypeSafeMatcher#matchesSafely(java.lang.Object)
	 */
	@Override
	protected boolean matchesSafely(final Double item) {
		return item == a * a;
	}

	@Override
	public void describeTo(final Description description) {
		description.appendValue(a * a);
	}

	@Factory
	public static TypeSafeMatcher<Double> aSqrtWith(final Double a) {
		return new SqrtMatcher(a);
	}
}
