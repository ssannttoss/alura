package com.ssannttoss.alura.java.tdd.general;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

public class ModelForMatchersTest {

	@Test
	public void test_startWithMatcher() {
		final ModelForMatchers mock = mock(ModelForMatchers.class);
		mock.setaString("Some how");
		verify(mock).setaString(startsWith("Some"));
	}

	@Test
	public void test_argumentCaptor() {
		final ModelForMatchers mock = mock(ModelForMatchers.class);
		final String frase = "That things are gona change";
		mock.setaString(frase);
		final ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
		verify(mock).setaString(argumentCaptor.capture());
		final String value = argumentCaptor.getValue();
		assertThat(value, equalTo(frase));

	}
}
