package com.ssannttoss.alura.java.tdd.math;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import com.ssannttoss.alura.java.tdd.service.ComplexOperations;

public class ComplexOperationsText {
	private ComplexOperations complexOperations;
	private Calculator calculator;

	@Before
	public void setUp() throws Exception {
		calculator = mock(Calculator.class);
		complexOperations = new ComplexOperations(calculator);
	}

	@Test
	public void max_a_b_should_return_a() throws DivideByZeroException {
		final double a = 3;
		final double b = 2;
		when(calculator.subtract(a, b)).thenReturn(a - b);
		assertThat(complexOperations.max(a, b), equalTo(a));
		verify(calculator).registry(a - b);
		verify(calculator, never()).divide(a, b);
	}

	@Test
	public void max_b_a_should_return_a() {
		final double a = 1;
		final double b = 0;
		when(calculator.subtract(b, a)).thenReturn(b - a);
		when(calculator.subtract(a, b)).thenReturn(a - b);
		assertThat(complexOperations.max(b, a), equalTo(a));
		verify(calculator, atMost(1)).registry(b - a);
		verify(calculator, atLeast(1)).registry(a - b);
	}

	@Test
	public void a_sqrt_of_a_should_be_like_a_multiplied_by_a() {
		final double a = 2;
		when(calculator.multiply(a, a)).then(invocationOnMock -> a * a);
		assertThat(complexOperations.sqrt(a), equalTo(a * a));
		verify(calculator, atLeastOnce()).registry(a * a);
	}

	@Test
	public void custom_matcher_a_sqrt_of_a_should_be_like_a_multiplied_by_a() {
		final double a = 2;
		when(calculator.multiply(a, a)).then(invocationOnMock -> a * a);
		assertThat(complexOperations.sqrt(a), SqrtMatcher.aSqrtWith(a));
		verify(calculator, times(1)).registry(a * a);
	}

	@Test
	public void verify_registry_called_once() {
		final double a = 2;
		final InOrder inOrder = inOrder(calculator);
		when(calculator.multiply(a, a)).then(invocationOnMock -> a * a);
		assertThat(complexOperations.sqrt(a), SqrtMatcher.aSqrtWith(a));
		inOrder.verify(calculator).reset();
		inOrder.verify(calculator, times(1)).registry(a * a);
	}

	@Test(expected = DivideByZeroException.class)
	public void division_by_zero_mock_exception() throws DivideByZeroException {
		final double a = 1;
		final double b = 0;
		doThrow(new DivideByZeroException("b cannot be zero")).when(calculator).divide(a, b);
		calculator.divide(a, b);
	}

	@Test(expected = DivideByZeroException.class)
	public void division_by_zero_mock_exception_with_any() throws DivideByZeroException {
		final double a = 1;
		final double b = 0;
		doThrow(new DivideByZeroException("b cannot be zero")).when(calculator).divide(any(Double.class), any(Double.class));
		calculator.divide(a, b);
	}
}
