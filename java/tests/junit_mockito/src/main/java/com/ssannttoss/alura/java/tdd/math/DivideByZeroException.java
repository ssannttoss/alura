package com.ssannttoss.alura.java.tdd.math;

public class DivideByZeroException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 *
	 * @param message
	 */
	public DivideByZeroException(final String message) {
		super(message);
	}

}
