package com.ssannttoss.alura.java.tdd.math;

/**
 * Simple calculator interface
 *
 * @author ssannttoss
 *
 */
public interface Calculator {
	/**
	 * Divides number a by b.
	 *
	 * @param a
	 * @param b
	 * @return
	 * @throws ArithmeticException
	 */
	double divide(double a, double b) throws DivideByZeroException;

	/**
	 * Multiplies number a by b.
	 *
	 * @param a fist number.
	 * @param b second number.
	 * @return a multiplied by b.
	 */
	double multiply(double a, double b);

	/**
	 * Subtracts number b from a
	 *
	 * @param a fist number.
	 * @param b second number.
	 * @return a minus b.
	 */
	double subtract(double a, double b);

	/**
	 * Sums two numbers.
	 *
	 * @param a fist number.
	 * @param b second number.
	 * @return a plus b.
	 */
	double sum(double a, double b);

	/**
	 * Stores the result of an operation in the history.
	 *
	 * @param result result of an operation.
	 * @returns the result value.
	 */
	double registry(final double result);

	/**
	 * Resets the history.
	 */
	void reset();
}
