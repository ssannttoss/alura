/**
 *
 */
package com.ssannttoss.alura.java.tdd.math;

/**
 * @author ssannttoss
 *
 */
public class MyCalculator implements Calculator {

	/*
	 * (non-Javadoc)
	 *
	 * @see com.ssannttoss.alura.java.tdd.math.Calculator#divide(double, double)
	 */
	@Override
	public double divide(final double a, final double b) throws DivideByZeroException {
		if (b == 0) {
			throw new DivideByZeroException("b number cannot be 0");
		}

		return a / b;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.ssannttoss.alura.java.tdd.math.Calculator#multiply(double, double)
	 */
	@Override
	public double multiply(final double a, final double b) {
		return a * b;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.ssannttoss.alura.java.tdd.math.Calculator#subtract(double, double)
	 */
	@Override
	public double subtract(final double a, final double b) {
		return a - b;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.ssannttoss.alura.java.tdd.math.Calculator#sum(double, double)
	 */
	@Override
	public double sum(final double a, final double b) {
		return a + b;
	}

	/**
	 *
	 */
	@Override
	public double registry(final double result) {
		System.out.println(result);
		return result;
	}

	@Override
	public void reset() {

	}

}
