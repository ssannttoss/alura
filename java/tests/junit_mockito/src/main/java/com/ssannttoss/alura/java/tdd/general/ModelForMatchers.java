package com.ssannttoss.alura.java.tdd.general;

import java.util.List;

import com.ssannttoss.alura.java.tdd.math.Calculator;

public class ModelForMatchers {
	private List<String> lstStrings;
	private String aString;

	public List<String> getLstStrings() {
		return lstStrings;
	}

	public void setLstStrings(final List<String> lstStrings) {
		this.lstStrings = lstStrings;
	}

	public String getaString() {
		return aString;
	}

	public void setaString(final String aString) {
		this.aString = aString;
	}

	public Calculator getCalculator() {
		return calculator;
	}

	public void setCalculator(final Calculator calculator) {
		this.calculator = calculator;
	}

	private Calculator calculator;
}
