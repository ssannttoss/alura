package com.ssannttoss.alura.java.tdd.service;

import com.ssannttoss.alura.java.tdd.math.Calculator;

/**
 * Uses a calculator to make complex operations.
 *
 * @author ssannttoss
 *
 */
public class ComplexOperations {
	private final Calculator calculator;

	public ComplexOperations(final Calculator calculator) {
		this.calculator = calculator;

	}

	/**
	 * Returns the max value.
	 *
	 * @param a first number.
	 * @param b second number.
	 * @return a if a is equals or greater than b, otherwise returns b.
	 */
	public double max(final double a, final double b) {
		calculator.reset();
		final double result1 = calculator.subtract(a, b);
		final double result2 = calculator.subtract(b, a);
		calculator.registry(result1);
		calculator.registry(result2);

		if (result1 < result2) {
			return b;
		}

		return a;
	}

	/**
	 * Returns a square root of a number.
	 *
	 * @param a number
	 * @return square root of a number.
	 */
	public double sqrt(final double a) {
		calculator.reset();
		final double result = calculator.multiply(a, a);
		calculator.registry(result);
		return result;
	}

}
