package br.com.alura.agenda.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

/**
 * Created by ssannttoss on 4/21/2018.
 */

public class AlunoPreferences {
    private static final String VERSAO = "versao";
    private final Context context;

    public AlunoPreferences(Context context) {
        this.context = context;
    }

    public boolean salvarVersao(String versao) {
        final SharedPreferences sharedPreferences = getSharedPreferences();
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(VERSAO, versao);
        return editor.commit();
    }

    public String getVersao() {
        final SharedPreferences sharedPreferences = getSharedPreferences();
        final String versao = sharedPreferences.getString(VERSAO, null);
        return versao;
    }

    private SharedPreferences getSharedPreferences() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(this.getClass().getSimpleName(), context.MODE_PRIVATE);
        return sharedPreferences;
    }
}
