package br.com.alura.agenda.sinc;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import br.com.alura.agenda.ListaAlunosActivity;
import br.com.alura.agenda.dao.AlunoDAO;
import br.com.alura.agenda.dto.AlunoSync;
import br.com.alura.agenda.event.AtualizaAgendaEvent;
import br.com.alura.agenda.modelo.Aluno;
import br.com.alura.agenda.preferences.AlunoPreferences;
import br.com.alura.agenda.retrofit.RetrofitInicializador;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlunoSincronizador {
    private final Context context;
    private final EventBus eventBus;

    public AlunoSincronizador(Context context) {
        this.context = context;
        this.eventBus = EventBus.getDefault();
    }

    public void buscaAlunos() {
        Call<AlunoSync> call = new RetrofitInicializador().getAlunoService().lista();

        call.enqueue(buscaAlunosCallback());
    }

    @NonNull
    private Callback<AlunoSync> buscaAlunosCallback() {
        return new Callback<AlunoSync>() {
            @Override
            public void onResponse(Call<AlunoSync> call, Response<AlunoSync> response) {
                final AlunoSync alunoSync = response.body();
                final String versao = alunoSync.getMomentoDaUltimaModificacao();
                if (new AlunoPreferences(context).salvarVersao(versao)) {
                    AlunoDAO dao = new AlunoDAO(context);
                    dao.sincroniza(alunoSync.getAlunos());
                    dao.close();
                }

                eventBus.post(new AtualizaAgendaEvent());
            }

            @Override
            public void onFailure(Call<AlunoSync> call, Throwable t) {
                eventBus.post(new AtualizaAgendaEvent());
                Log.e("onFailure chamado", t.getMessage());
            }
        };
    }

    public void buscaNovos() {
        final String versao = new AlunoPreferences(context).getVersao();
        if (versao != null) {
            Call<AlunoSync> call = new RetrofitInicializador().getAlunoService().buscaNovos(versao);
            call.enqueue(buscaAlunosCallback());
        } else {
            buscaAlunos();
        }
    }

    public void sincronizarAlunosInternos() {
        final AlunoDAO alunoDAO = new AlunoDAO(context);
        final List<Aluno> alunos = alunoDAO.obterNaoSincronizados();
        final Call<AlunoSync> call = new RetrofitInicializador().getAlunoService().enviarNaoSincronizados(alunos);
        call.enqueue(new Callback<AlunoSync>() {
            @Override
            public void onResponse(Call<AlunoSync> call, Response<AlunoSync> response) {

            }

            @Override
            public void onFailure(Call<AlunoSync> call, Throwable t) {

            }
        });
    }
}