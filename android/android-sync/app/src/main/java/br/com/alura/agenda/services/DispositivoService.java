package br.com.alura.agenda.services;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by ssannttoss on 4/15/2018.
 */

public interface DispositivoService {

    @POST("firebase/dispositivo")
    Call<Void> enviarToken(@Header("token") String token);
}
