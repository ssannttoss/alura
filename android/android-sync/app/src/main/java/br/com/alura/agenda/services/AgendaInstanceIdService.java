package br.com.alura.agenda.services;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import br.com.alura.agenda.retrofit.RetrofitInicializador;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ssannttoss on 4/15/2018.
 */

public class AgendaInstanceIdService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("onTokenRefresh", "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(final String refreshedToken) {
        //eJSI5CPZk5o:APA91bHN8ypXWBcyNAmi4tfvW72gYOx8QwRc98GmEQNG2rLIH-3pRCPf2EU_pWI5C1oMEssdFh4vQrMsfKPLmzprBtK-iMFiuWjM16Zu0Q36QBAQ4UgLAuYRYQDnPP9PNskkxrvg-pPZ
        Call<Void> call = new RetrofitInicializador().getDispositivoService().enviarToken(refreshedToken);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i("onResponse", "Token enviado: " + refreshedToken);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
            }
        });
    }
}
