package br.com.alura.agenda.services;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.Map;

import br.com.alura.agenda.dao.AlunoDAO;
import br.com.alura.agenda.dto.AlunoSync;
import br.com.alura.agenda.event.AtualizaAgendaEvent;

/**
 * Created by ssannttoss on 4/15/2018.
 */

public class AgendaMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d("onMessageReceived", "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d("onMessageReceived", "Message data payload: " + remoteMessage.getData());

            Map<String, String> mensagem = remoteMessage.getData();
            Log.i("mensagem recebida", String.valueOf(mensagem));
            converterParaAluno(mensagem);

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d("onMessageReceived", "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void converterParaAluno(Map<String, String> mensagem) {
        final String key = "alunoSync";
        if (mensagem.containsKey(key)) {
            final String json = mensagem.get(key);
            final ObjectMapper mapper = new ObjectMapper();

            try {
                final AlunoSync alunoSync = mapper.readValue(json, AlunoSync.class);
                final AlunoDAO alunoDAO = new AlunoDAO(this);
                alunoDAO.sincroniza(alunoSync.getAlunos());
                alunoDAO.close();

                EventBus eventBus = EventBus.getDefault();
                eventBus.post(new AtualizaAgendaEvent());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
