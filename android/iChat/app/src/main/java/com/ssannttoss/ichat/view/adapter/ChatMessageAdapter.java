package com.ssannttoss.ichat.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.ssannttoss.ichat.App;
import com.ssannttoss.ichat.R;
import com.ssannttoss.ichat.model.ChatMessage;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ssannttoss on 4/5/2018.
 */
public class ChatMessageAdapter extends RecyclerView.Adapter {
    private final Context context;
    private final List<ChatMessage> chatMessageList;
    private final int clientId;
    @Inject
    Picasso picasso;

    public ChatMessageAdapter(Context context, int clientId, List<ChatMessage> chatMessageList) {
        this.context = context;
        this.chatMessageList = chatMessageList;
        this.clientId = clientId;
        App app = (App)context.getApplicationContext();
        app.getChatComponent().inject(this);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        final View view = layoutInflater.inflate(R.layout.chat_message_item, parent, false);
        RecyclerView.ViewHolder viewHolder = new ChatMessageViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ChatMessage chatMessage = chatMessageList.get(position);
        ChatMessageViewHolder chatMessageViewHolder = (ChatMessageViewHolder)holder;

        chatMessageViewHolder.tvwMensagem.setText(chatMessage.getContent());
        if (clientId != chatMessage.getClientId()) {
            chatMessageViewHolder.view.setBackgroundColor(Color.CYAN);
        }

        picasso .load(context.getString(R.string.adorable_api) + chatMessage.getClientId() + ".png")
                .into(chatMessageViewHolder.ivwAvatar);
    }

    @Override
    public int getItemCount() {
        return chatMessageList.size();
    }


    static final class ChatMessageViewHolder extends RecyclerView.ViewHolder {
        public View view;

        @BindView(R.id.tvw_chat_message)
        TextView tvwMensagem;
        @BindView(R.id.ivw_chat_avatar)
        AppCompatImageView ivwAvatar;

        public ChatMessageViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            ButterKnife.bind(this, itemView);
        }
    }
}
