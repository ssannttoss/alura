package com.ssannttoss.ichat.service;

import com.ssannttoss.ichat.model.ChatMessage;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by ssannttoss on 4/7/2018.
 */

public interface ChatService {
    @POST("polling")
    Call<Void> sendMessage(@Body final ChatMessage chatMessage);

    @GET("polling")
    Call<ChatMessage> checkMessages();
}
