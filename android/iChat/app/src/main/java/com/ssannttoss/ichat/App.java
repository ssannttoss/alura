package com.ssannttoss.ichat;

import android.app.Application;

import com.ssannttoss.ichat.di.ChatComponent;
import com.ssannttoss.ichat.di.ChatModule;
import com.ssannttoss.ichat.di.DaggerChatComponent;

/**
 * Created by ssannttoss on 4/8/2018.
 */

public class App extends Application {
    private ChatComponent chatComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        chatComponent = DaggerChatComponent
                .builder()
                .chatModule(new ChatModule(this))
                .build();
    }

    public ChatComponent getChatComponent() {
        return chatComponent;
    }
}
