package com.ssannttoss.ichat.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.ssannttoss.ichat.App;
import com.ssannttoss.ichat.R;
import com.ssannttoss.ichat.model.ChatMessage;
import com.ssannttoss.ichat.service.ChatCallBack;
import com.ssannttoss.ichat.service.ChatFailure;
import com.ssannttoss.ichat.service.ChatNewMessageEventArgs;
import com.ssannttoss.ichat.service.ChatService;
import com.ssannttoss.ichat.view.adapter.ChatMessageAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.util.ThrowableFailureEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class MainActivity extends AppCompatActivity {
    public static final String MESSAGES_KEY = "messages";
    public static final String CLIEND_ID_KEY = "cliendId";
    private final List<ChatMessage> chatMessageList;
    private int clientId = UUID.randomUUID().hashCode();
    @Inject
    public ChatService chatService; //Dagger needs a public field

    @BindView(R.id.btn_chat_send_message)
    AppCompatButton btnSend;
    @BindView(R.id.rcv_messages)
    RecyclerView rcvChatMessages;
    @BindView(R.id.edt_chat_new_message)
    AppCompatEditText edtMessage;
    @BindView(R.id.ivw_client_avatar)
    AppCompatImageView ivwAvatar;
    @Inject
    Picasso picasso;
    @Inject
    EventBus eventBus;
    @Inject
    InputMethodManager inputMethodManager;

    public MainActivity() {
        super();
        this.chatMessageList = new ArrayList<>();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (savedInstanceState != null) {
            List<ChatMessage> savedChatMessageList = savedInstanceState.getParcelableArrayList(MESSAGES_KEY);
            if (savedChatMessageList != null) {
                chatMessageList.addAll(savedChatMessageList);
            }

            if (savedInstanceState.containsKey(CLIEND_ID_KEY)) {
                clientId = savedInstanceState.getInt(CLIEND_ID_KEY, UUID.randomUUID().hashCode());
            }
        }

        displayMessages();

        App app = (App)getApplication();
        app.getChatComponent().inject(this);

        /*LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(receiver, new IntentFilter("new_message"));*/
        eventBus.register(this);
        listenChatMessages(null);

        picasso.load(getString(R.string.adorable_api) + clientId + ".png").into(ivwAvatar);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(MESSAGES_KEY, (ArrayList<? extends Parcelable>) chatMessageList);
        outState.putInt(CLIEND_ID_KEY, clientId);
    }

    @Override
    protected void onDestroy() {
        /*LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.unregisterReceiver(receiver);*/
        eventBus.unregister(this);
        super.onDestroy();
    }

    @OnClick(R.id.btn_chat_send_message)
    public void sendMessage() {
        final ChatMessage chatMessage = new ChatMessage(clientId, edtMessage.getText().toString());
        Call<Void> callSendMessage = chatService.sendMessage(chatMessage);
        callSendMessage.enqueue(new ChatCallBack.SendMessage(this, eventBus));
        edtMessage.setText("");
        inputMethodManager.hideSoftInputFromWindow(edtMessage.getWindowToken(), 0);
    }

    private void displayMessages() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
                //linearLayoutManager.setStackFromEnd(true);
                //linearLayoutManager.setReverseLayout(true);
                rcvChatMessages.setLayoutManager(linearLayoutManager);
                final ChatMessageAdapter adapter = new ChatMessageAdapter(MainActivity.this, clientId, chatMessageList);
                rcvChatMessages.setAdapter(adapter);
                rcvChatMessages.scrollToPosition(chatMessageList.size() - 1);
            }
        });

    }

    @Subscribe
    public void onNewMessage(ChatNewMessageEventArgs chatNewMessageEventArgs) {
        chatMessageList.add(chatNewMessageEventArgs.getChatMessage());
        displayMessages();
        listenChatMessages(null);
    }

    @Subscribe
    private void listenChatMessages(ChatNewMessageEventArgs chatNewMessageEventArgs) {
        Call<ChatMessage> chatMessageCall = this.chatService.checkMessages();
        chatMessageCall.enqueue(new ChatCallBack.ReceiveMessage(this, eventBus));
    }

    @Subscribe
    private void onFailure(ChatFailure chatFailure) {
        if (chatFailure.getThrowable() != null) {
            Toast.makeText(this, chatFailure.getThrowable().getLocalizedMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Unexpected error", Toast.LENGTH_LONG).show();
        }

        listenChatMessages(null);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ChatNewMessageEventArgs chatNewMessageEventArgs = intent.getParcelableExtra("args");
            onNewMessage(chatNewMessageEventArgs);
        }
    };
}
