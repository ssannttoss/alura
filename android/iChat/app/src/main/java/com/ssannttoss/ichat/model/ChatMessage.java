package com.ssannttoss.ichat.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ssannttoss on 4/5/2018.
 */

public class ChatMessage implements Parcelable {
    @SerializedName("id")
    private final int clientId;
    @SerializedName("text")
    private final String content;

    public ChatMessage(int clientId, String content) {
        this.clientId = clientId;
        this.content = content;
    }

    protected ChatMessage(Parcel in) {
        clientId = in.readInt();
        content = in.readString();
    }

    public static final Creator<ChatMessage> CREATOR = new Creator<ChatMessage>() {
        @Override
        public ChatMessage createFromParcel(Parcel in) {
            return new ChatMessage(in);
        }

        @Override
        public ChatMessage[] newArray(int size) {
            return new ChatMessage[size];
        }
    };

    public int getClientId() {
        return clientId;
    }

    public String getContent() {
        return content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(clientId);
        dest.writeString(content);
    }
}
