package com.ssannttoss.ichat.service;

import android.content.Context;

import com.ssannttoss.ichat.App;
import com.ssannttoss.ichat.model.ChatMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.util.ThrowableFailureEvent;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ssannttoss on 4/9/2018.
 */

public class ChatCallBack {
    public static final class ReceiveMessage implements Callback<ChatMessage> {
        private final Context context;
        private final EventBus eventBus;

        public ReceiveMessage(Context context, EventBus eventBus) {
            this.context = context;
            this.eventBus = eventBus;
        }

        @Override
        public void onResponse(Call<ChatMessage> call, Response<ChatMessage> response) {
            //Intent newMessageIntent = new Intent("new_message");
            ChatNewMessageEventArgs chatNewMessageEventArgs;
            if (response.isSuccessful()) {
                ChatMessage chatMessage = response.body();
                chatNewMessageEventArgs = new ChatNewMessageEventArgs(chatMessage);
                //newMessageIntent.putExtra("new_message", chatMessage);
                eventBus.post(chatNewMessageEventArgs);
            } else {
                eventBus.post(new ChatFailure(new Exception()));
            }

            //LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
            //localBroadcastManager.sendBroadcast(newMessageIntent);
        }

        @Override
        public void onFailure(Call<ChatMessage> call, Throwable t) {
            /*Intent newMessageFailureIntent = new Intent("exception");
            newMessageFailureIntent.putExtra("exception", t);
            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
            localBroadcastManager.sendBroadcast(newMessageFailureIntent);*/
            eventBus.post(new ChatFailure(t));
        }
    }

    public static final class SendMessage implements Callback<Void> {
        private final Context context;
        private final EventBus eventBus;

        public SendMessage(Context context, EventBus eventBus) {
            this.context = context;
            this.eventBus = eventBus;
        }

        @Override
        public void onResponse(Call<Void> call, Response<Void> response) {

        }

        @Override
        public void onFailure(Call<Void> call, Throwable t) {

        }
    }
}
