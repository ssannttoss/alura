package com.ssannttoss.ichat.di;

import com.ssannttoss.ichat.service.ChatCallBack;
import com.ssannttoss.ichat.view.MainActivity;
import com.ssannttoss.ichat.view.adapter.ChatMessageAdapter;

import dagger.Component;
import retrofit2.Callback;

/**
 * Created by ssannttoss on 4/8/2018.
 */

@Component(modules = ChatModule.class)
public interface ChatComponent {
    void inject(MainActivity activity);
    void inject(ChatMessageAdapter adapter);
}
