package com.ssannttoss.ichat.service;

import com.ssannttoss.ichat.model.ChatMessage;

/**
 * Created by ssannttoss on 4/9/2018.
 */

public class ChatNewMessageEventArgs {
    private final ChatMessage chatMessage;

    public ChatNewMessageEventArgs(ChatMessage chatMessage) {
        this.chatMessage = chatMessage;
    }


    public ChatMessage getChatMessage() {
        return chatMessage;
    }


}
