package com.ssannttoss.ichat.service;

import java.io.Serializable;

/**
 * Created by ssannttoss on 4/10/2018.
 */

public class ChatFailure {
    private final Throwable throwable;

    public ChatFailure(Throwable throwable) {
        this.throwable = throwable;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
