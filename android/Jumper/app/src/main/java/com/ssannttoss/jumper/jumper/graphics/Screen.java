package com.ssannttoss.jumper.jumper.graphics;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by ssannttoss on 3/24/2018.
 */

public class Screen {

    private final Context mContext;
    private final DisplayMetrics mMetrics;
    private static final int FLOOR_HEIGTH = 150;

    public Screen(Context context) {
        mContext = context;
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        mMetrics = new DisplayMetrics();
        display.getMetrics(mMetrics);
    }

    public int getHeight() {
        return mMetrics.heightPixels;
    }

    public int getGameAreaTop() {
        return 0;
    }

    public int getGameAreaBottom() {
        return mMetrics.heightPixels - FLOOR_HEIGTH;
    }

    public int getGameAreaLeft() {
        return 0;
    }

    public int getGameAreaRight() {
        return mMetrics.widthPixels;
    }
}
