package com.ssannttoss.jumper.jumper;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.ssannttoss.jumper.jumper.engine.Game;

public class MainActivity extends AppCompatActivity {

    private Game mGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mGame = new Game(this);
        FrameLayout container = findViewById(R.id.container);
        container.addView(mGame);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        new Thread(mGame).start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGame.pause();
    }
}
