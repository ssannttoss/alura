package com.ssannttoss.jumper.jumper.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.DisplayMetrics;

import com.ssannttoss.jumper.jumper.R;

/**
 * Created by ssannttoss on 3/24/2018.
 */

public class Background {

    private final Context mContext;
    private final Bitmap mBackground;

    public Background(Screen screen, Context context) {
        mContext = context;
        Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.background);
        mBackground = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), screen.getHeight(), false);
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(mBackground, 0, 0, null);
    }
}
