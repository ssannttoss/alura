package com.ssannttoss.jumper.jumper.graphics;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;

import com.ssannttoss.jumper.jumper.engine.Player;

/**
 * Created by ssannttoss on 3/24/2018.
 */

public class CountDown {
    private final Paint mPaint;
    private final Screen mScreen;
    private final Player mPlayer;
    private int mCountDown;
    private final Rect mTextBounds;

    public CountDown(Screen screen, Player player, int countDown) {
        mScreen = screen;
        mPlayer = player;
        mCountDown = countDown;
        mPaint = new Paint();
        mPaint.setColor(0xFFFFFFFF);
        mPaint.setTextSize(200);
        mPaint.setTypeface(Typeface.DEFAULT_BOLD);
        mPaint.setShadowLayer(2, 3, 3, 0xFF000000);
        mTextBounds = new Rect();
        mPaint.getTextBounds("0", 0, 1, mTextBounds);
    }

    public boolean run(Canvas canvas) {
        if (mCountDown - 1 >= 0) {
            mPlayer.play(Player.SCORE);
            canvas.drawText(String.valueOf(mCountDown), (mScreen.getGameAreaRight() - mTextBounds.width()) / 2, mScreen.getGameAreaBottom() / 2, mPaint);
            mCountDown--;
            return true;
        } else {
            return false;
        }
    }
}
