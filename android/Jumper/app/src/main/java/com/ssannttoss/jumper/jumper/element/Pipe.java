package com.ssannttoss.jumper.jumper.element;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.ssannttoss.jumper.jumper.graphics.Screen;

/**
 * Created by ssannttoss on 3/24/2018.
 */

public class Pipe {
    public static final int MIN_HEIGHT = 250;
    public static final int WIDTH = 100;
    private static final int MOVE = 5;
    private static final Paint sColor;
    private final Screen mScreen;
    private final boolean mBottom;
    private final Rect mRect;
    private int mPosition;
    private int mHeight;

    static {
        sColor = new Paint();
        sColor.setColor(0xFF00FF00);
        sColor.setShadowLayer(10, 10, -10, 0x38393838);
    }

    public Pipe(Screen screen, int initialPosition, boolean bottom) {
        mScreen = screen;
        mBottom = bottom;
        mRect = new Rect();
        updateHeight();
        mPosition = initialPosition;
    }

    public void draw(Canvas canvas) {
        if (mBottom) {
            mRect.set(mPosition, mHeight, mPosition + WIDTH, mScreen.getGameAreaBottom());
            canvas.drawRect(mRect, sColor);
        } else {
            mRect.set(mPosition, mScreen.getGameAreaTop(), mPosition + WIDTH, mHeight);
            canvas.drawRect(mRect, sColor);
        }
    }

    public void resetPosition(int newPosition) {
        mPosition = newPosition;
        updateHeight();
    }

    public int getPosition() {
        return mPosition;
    }

    public void move() {
        mPosition -= MOVE;
    }

    public boolean isOnScreen() {
        return (mPosition + WIDTH) > mScreen.getGameAreaLeft();
    }

    public boolean isAtBottom() {
        return mBottom;
    }

    private void updateHeight() {
        if (!isOnScreen() || mHeight == 0) {
            int random = (int) (Math.random() * mScreen.getGameAreaBottom() / 2);
            if (random < MIN_HEIGHT) {
                random = MIN_HEIGHT;
            }

            if (mBottom) {
                mHeight = mScreen.getGameAreaBottom() - random;
            } else {
                mHeight = mScreen.getGameAreaTop() + random;
            }
        }
    }

    public Rect getRect() {
        return mRect;
    }
}
