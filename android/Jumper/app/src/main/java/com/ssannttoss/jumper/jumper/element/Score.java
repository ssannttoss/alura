package com.ssannttoss.jumper.jumper.element;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;

import com.ssannttoss.jumper.jumper.engine.Player;
import com.ssannttoss.jumper.jumper.graphics.Screen;

/**
 * Created by ssannttoss on 3/24/2018.
 */

public class Score {
    private static final int SCORE_INCREMENT = 1;
    private static final Paint sColor;
    private final Screen mScreen;
    private final Player mPlayer;
    private int mCurrentStore;

    static {
        sColor = new Paint();
        sColor.setColor(0xFFFFFFFF);
        sColor.setTextSize(80);
        sColor.setTypeface(Typeface.DEFAULT_BOLD);
        sColor.setShadowLayer(3, 5, 5, 0xFF000000);
    }

    public Score(Screen screen, Player player) {
        mScreen = screen;
        mPlayer = player;
    }

    public void increase() {
        mCurrentStore += SCORE_INCREMENT;
        mPlayer.play(Player.SCORE);
    }

    public void draw(Canvas canvas) {
        canvas.drawText(String.valueOf(mCurrentStore), mScreen.getGameAreaLeft() + 100, mScreen.getGameAreaTop() + 100, sColor);
    }
}
