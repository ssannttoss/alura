package com.ssannttoss.jumper.jumper.engine;

/**
 * Created by ssannttoss on 3/24/2018.
 */

public interface YieldReturn<T> {
    boolean onReturn(T value);
}
