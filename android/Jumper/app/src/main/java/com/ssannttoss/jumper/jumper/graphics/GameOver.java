package com.ssannttoss.jumper.jumper.graphics;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;

/**
 * Created by ssannttoss on 3/24/2018.
 */

public class GameOver {
    private final Paint mPaint;
    private final Screen mScreen;
    private static final String GAME_OVER = "GAME OVER";
    private final Rect mTextBounds;

    public GameOver(Screen screen) {
        mScreen = screen;
        mPaint = new Paint();
        mPaint.setColor(0xFFFF0000);
        mPaint.setTextSize(100);
        mPaint.setTypeface(Typeface.DEFAULT_BOLD);
        mPaint.setShadowLayer(2, 3, 3, 0xFF000000);
        mTextBounds = new Rect();
        mPaint.getTextBounds(GAME_OVER, 0, GAME_OVER.length(), mTextBounds);
    }

    public void draw(Canvas canvas) {
        canvas.drawText("Game Over", (mScreen.getGameAreaRight() - mTextBounds.width()) / 2, mScreen.getGameAreaBottom() /2, mPaint);
    }
}
