package com.ssannttoss.jumper.jumper.element;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.ssannttoss.jumper.jumper.R;
import com.ssannttoss.jumper.jumper.engine.Player;
import com.ssannttoss.jumper.jumper.graphics.Screen;

/**
 * Created by ssannttoss on 3/24/2018.
 */

public class Jumper {
    public static final int X = 150;
    public static final int RADIUS = 100;
    public static final int LEFT = X - RADIUS;
    public static final int RIGHT = X + RADIUS;
    private static final int FALL = 5;
    private static final int JUMNP = 150;
    private final Screen mScreen;
    private final Bitmap mImage;
    private final Player mPlayer;
    private int mHigth;

    public Jumper(Screen screen, Resources resources, Player player) {
        mPlayer = player;
        Bitmap bitmap = BitmapFactory.decodeResource(resources, R.drawable.jumper);
        mImage = bitmap.createScaledBitmap(bitmap, RADIUS * 2, RADIUS * 2, false);
        bitmap.recycle();
        mScreen = screen;
        mHigth = 100;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(mImage, X - RADIUS, mHigth - RADIUS, null);
    }

    public void jump() {
        if (mHigth - JUMNP > RADIUS) {
            mHigth -= JUMNP;
            mPlayer.play(Player.JUMP);
        }
    }

    public void fall() {
        boolean hitFloor = mHigth + RADIUS > mScreen.getGameAreaBottom();

        if (!hitFloor) {
            mHigth += FALL;
        }
    }

    public int getHigth() {
        return mHigth;
    }
}
