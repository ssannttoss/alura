package com.ssannttoss.jumper.jumper.element;

import android.graphics.Canvas;

import com.ssannttoss.jumper.jumper.engine.YieldReturn;
import com.ssannttoss.jumper.jumper.graphics.Screen;

import java.util.ArrayList;

/**
 * Created by ssannttoss on 3/24/2018.
 */

public class Pipes {

    private static final int PIPE_COUNT = 10;
    private static final int PIPE_SPACE = 250;
    private final Screen mScreen;
    private final ArrayList<Pipe> mPipes;
    private final YieldReturn mYieldReturn;
    private static final int PIPE_SHIFT = Pipe.WIDTH + PIPE_SPACE;

    public Pipes(Screen screen, YieldReturn yieldReturn) {
        mScreen = screen;
        mYieldReturn = yieldReturn;
        mPipes = new ArrayList<>(PIPE_COUNT);
        int screenRight = mScreen.getGameAreaRight() + PIPE_SPACE;

        for(int i = 0; i < PIPE_COUNT; i++) {
            int initialPosition = screenRight + (PIPE_SPACE * i);
            Pipe pipe = new Pipe(mScreen, initialPosition, i % 2 == 0);
            mPipes.add(pipe);
        }
    }
    
    public void draw(Canvas canvas) {
        for(Pipe pipe : mPipes) {
            pipe.draw(canvas);
        }
    }

    public boolean move() {
        for(int i = 0; i < mPipes.size(); i++) {
            Pipe pipe = mPipes.get(i);

            if (!pipe.isOnScreen()) {
                int newPosition = lastPipePosition(pipe.isAtBottom()) + PIPE_SHIFT;
                pipe.resetPosition(newPosition);
            } else {
                pipe.move();
            }

            if (!mYieldReturn.onReturn(pipe)) {
                return false;
            }
        }

        return true;
    }

    private int lastPipePosition(boolean atBottom) {
        int lastPosition = 0;

        for(int i = 0; i < mPipes.size(); i++) {
            Pipe pipe = mPipes.get(i);

            if (pipe.isAtBottom() == atBottom) {
                lastPosition = Math.max(pipe.getPosition(), lastPosition);
            }
        }

        return lastPosition;
    }
}
