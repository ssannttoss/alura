package com.ssannttoss.jumper.jumper.engine;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;

import com.ssannttoss.jumper.jumper.R;

/**
 * Created by ssannttoss on 3/24/2018.
 */

public class Player {
    public static int JUMP;
    public static int COLLISION;
    public static int SCORE;
    private final SoundPool mSoundPool;
    private final Context mContext;
    private static final int LEFT_VOLUME = 1;
    private static final int RIGHT_VOLUME = 1;
    private static final int PRIORITY = 1;
    private static final int RATE = 1;

    public Player(Context context) {
        mContext = context;

        if (Build.VERSION.SDK_INT > 21) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC).build();
            mSoundPool = new SoundPool.Builder()
                    .setMaxStreams(3)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            mSoundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
        }

        JUMP = mSoundPool.load(mContext, R.raw.jump, 1);
        COLLISION = mSoundPool.load(mContext, R.raw.collision, 1);
        SCORE = mSoundPool.load(mContext, R.raw.score, 1);
    }

    public void play(int soundId) {
        mSoundPool.play(soundId, LEFT_VOLUME, RIGHT_VOLUME, PRIORITY, 0, RATE);
    }
}
