package com.ssannttoss.jumper.jumper.engine;

import android.graphics.Rect;

import com.ssannttoss.jumper.jumper.element.Jumper;
import com.ssannttoss.jumper.jumper.element.Pipe;

/**
 * Created by ssannttoss on 3/24/2018.
 */

public class CollisionDetector {

    public boolean hasCollision(Jumper jumper, Pipe pipe) {
        Rect rect = pipe.getRect();
        boolean horizonalCollision = rect.left <= Jumper.RIGHT && rect.right > Jumper.LEFT;
        boolean verticalCollision = false;

        if (horizonalCollision) {
            if (pipe.isAtBottom()) {
                verticalCollision = rect.top <= jumper.getHigth() + Jumper.RADIUS;
            } else {
                verticalCollision = rect.bottom >= jumper.getHigth() - Jumper.RADIUS;
            }
        }

        return horizonalCollision && verticalCollision;
    }
}
