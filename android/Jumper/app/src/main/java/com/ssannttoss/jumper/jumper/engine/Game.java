package com.ssannttoss.jumper.jumper.engine;

import android.content.Context;
import android.graphics.Canvas;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.ssannttoss.jumper.jumper.element.Jumper;
import com.ssannttoss.jumper.jumper.element.Pipe;
import com.ssannttoss.jumper.jumper.element.Pipes;
import com.ssannttoss.jumper.jumper.element.Score;
import com.ssannttoss.jumper.jumper.graphics.Background;
import com.ssannttoss.jumper.jumper.graphics.CountDown;
import com.ssannttoss.jumper.jumper.graphics.GameOver;
import com.ssannttoss.jumper.jumper.graphics.Screen;


/**
 * Created by ssannttoss on 3/24/2018.
 */

public class Game extends SurfaceView implements Runnable, View.OnTouchListener, YieldReturn<Pipe> {

    private boolean mIsRunning = false;
    private boolean mCanExecuted;
    private final Context mContext;
    private final SurfaceHolder mHolder;
    private Jumper mJumper;
    private Background mBackground;
    private Screen mScreen;
    private Pipes mPipes;
    private Score mScore;
    private CollisionDetector mCollisionDetector;
    private Player mPlayer;
    private CountDown mCountDown;

    public Game(Context context) {
        super(context);
        mContext = context;
        mHolder = getHolder();
        mHolder.setKeepScreenOn(true);
        initElements();
        setOnTouchListener(this);
        mCanExecuted = true;
    }

    private void initElements() {
        mScreen = new Screen(mContext);
        mPlayer = new Player(mContext);
        mScore = new Score(mScreen, mPlayer);
        mJumper = new Jumper(mScreen, mContext.getResources(), mPlayer);
        mPipes = new Pipes(mScreen, this);
        mBackground = new Background(mScreen, mContext);
        mCollisionDetector = new CollisionDetector();
        mCountDown = new CountDown(mScreen, mPlayer, 5);
    }

    @Override
    public void run() {
        while(mCanExecuted) {
            if (!mHolder.getSurface().isValid()) {
                continue;
            }

            Canvas canvas = mHolder.lockCanvas();
            mBackground.draw(canvas);

            if (mIsRunning) {
                mPipes.draw(canvas);
                boolean keepMoving = mPipes.move();
                mScore.draw(canvas);
                mJumper.draw(canvas);

                if (keepMoving) {
                    mJumper.fall();
                } else {
                    new GameOver(mScreen).draw(canvas);
                }
                mHolder.unlockCanvasAndPost(canvas);
            } else {
                mIsRunning = !mCountDown.run(canvas);
                mHolder.unlockCanvasAndPost(canvas);
                SystemClock.sleep(1000);
            }
        }
    }

    public synchronized void pause() {
        mIsRunning = false;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (mIsRunning) {
            mJumper.jump();
        }

        return false;
    }

    @Override
    public boolean onReturn(Pipe value) {
        if (value.isOnScreen()) {
            if (mCollisionDetector.hasCollision(mJumper, value)) {
                mPlayer.play(Player.COLLISION);
                mCanExecuted = false;
            }
        } else {
            mScore.increase();
        }

        return mCanExecuted;
    }
}
