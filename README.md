# Alura - Online Courses #

This repository is used to keep tracking of my exercises on Alura courses and is not intended to be used as portfolio or to do code evaluation once the most of the time I'm following the course guideline.

For a complete list of the courses I have accomplished, please access my [Full Certificate](https://cursos.alura.com.br/user/ssannttoss/fullCertificate/95266da9313a7ff77098ebbaa0a7772e)