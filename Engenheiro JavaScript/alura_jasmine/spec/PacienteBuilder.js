let p = require('../Paciente');

exports.PacienteBuilder = function() {
    var nome = 'Mario';
    var idade = 34;
    var peso = 72;
    var altura = 1.83;

    var clazz = {
        build : function() {
            let paciente = new p.Paciente(nome, idade, peso, altura);
            return paciente;
        },

        withIdade : function(value) {
            this.idade = value;
            return this;
        }
    };

    return clazz;
}