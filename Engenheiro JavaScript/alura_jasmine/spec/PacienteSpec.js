let p = require('../Paciente');
let c = require('../Consulta');
let b = require('./PacienteBuilder');
let a = require('../Agendamento');
require('jasmine');

describe('Paciente', function() {
    var paciente = null;
    beforeEach(function() {
        paciente = new b.PacienteBuilder().build();
    })

    describe('Saúde paciente', function() {
        if('deve calcular o IMC', function(done) {
            expect(paciente.imc()).toEqual(72 / (Math.pow(1.83, 2)));
            done();
        });

        if('deve calcular os batimentos', function(done) {
            expect(paciente.batimentos()).toEqual(34 * 365 * 24 * 60 * 80);
            done();
        });
    });

    describe('Consulta', function() {

        it("deve cobrar 25 por cada procedimento comum", function() {
            var consulta = new c.Consulta(paciente, ["proc1", "proc2"], false, false);
            expect(consulta.preco()).toEqual(50);
        });

        it("deve dobrar o preco da consulta particular", function() {
            var consulta = new c.Consulta(paciente, ["abc", "xyz"], true, false);
            expect(consulta.preco()).toEqual(50 * 2);
        });

        it("deve cobrar preco especifico dependendo do procedimento", function() {
            var consulta = new c.Consulta(paciente, ["procedimento-comum", "raio-x", "procedimento-comum2", "gesso"], false, false);
            expect(consulta.preco()).toEqual(25 + 55 + 25 + 32);
        });

        it("deve cobrar em dobro o preco especifico dependendo do procedimento", function() {
            var consulta = new c.Consulta(paciente, ["raio-x", "gesso"], true, false);
            expect(consulta.preco()).toEqual((55 + 32)*2);
        });

        it("não deve cobrar por consulta de retorno", function() {
            var consulta = new c.Consulta(paciente, [], true, true);
            expect(consulta.preco()).toEqual(0);
        });
    });

    describe('Agendamento', function() {
        var agenda;
        var paciente;
        beforeEach(function() {
            agenda = new a.Agendamento();
            paciente = new b.PacienteBuilder().build();
        })

        it("deve agendar para 20 dias depois", function() {
            var consulta = new c.Consulta(paciente, [], false, false, new Date(2014, 7, 1));
            var novaConsulta = agenda.para(consulta);

            expect(novaConsulta.getData().toString()).toEqual(new Date(2014,7,21).toString());

        });

        it("deve pular fins de semana", function() {
            var consulta = new c.Consulta(paciente, [], false, false, new Date(2014, 5, 30));
            var novaConsulta = agenda.para(consulta);
            expect(novaConsulta.getData().toString()).toEqual(new Date(2014,6,21).toString());
        });
    });
})