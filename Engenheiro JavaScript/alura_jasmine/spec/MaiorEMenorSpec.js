let fn = require('../MaiorEMenor');

describe('maior e menor', function() {
    it('deve entender números em ordem não sequencial', function() {
        var algoritmo = new fn.MaiorEMenor();
        algoritmo.encontra([16, 5,15,7,9]);
        expect(algoritmo.pegaMenor()).toEqual(5);
        expect(algoritmo.pegaMaior()).toEqual(16);
    });

    it('deve entender números em ordem crescenter', function() {
        var algoritmo = new fn.MaiorEMenor();
        algoritmo.encontra([3,5,7,11,13,17]);
        expect(algoritmo.pegaMenor()).toEqual(3);
        expect(algoritmo.pegaMaior()).toEqual(17);
    });

    it('deve entender números em ordem decrescente', function() {
        var algoritmo = new fn.MaiorEMenor();
        algoritmo.encontra([19,17,13,11,7,5,3,1]);
        expect(algoritmo.pegaMenor()).toEqual(1);
        expect(algoritmo.pegaMaior()).toEqual(19);
    });

    it('deve entender apenas 1 número', function() {
        var algoritmo = new fn.MaiorEMenor();
        algoritmo.encontra([191]);
        expect(algoritmo.pegaMenor()).toEqual(191);
        expect(algoritmo.pegaMaior()).toEqual(191);
    });
})