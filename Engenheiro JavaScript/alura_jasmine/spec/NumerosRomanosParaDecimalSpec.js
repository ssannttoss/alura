let n = require('../NumerosRomanosParaDecimal');

describe('NumeroRomanosParaDecimal', function() {
    if('converte para decimal', function() {
        var conversor = new n.RomanosParaDecimal();
        expect(conversor.convert("I")).toEqual(1);
        expect(conversor.convert("IV")).toEqual(4);
        expect(conversor.convert("XVI")).toEqual(16);
        expect(conversor.convert("X")).toEqual(10);
    });
});