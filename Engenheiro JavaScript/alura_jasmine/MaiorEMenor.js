exports.MaiorEMenor = function() {

    var maior = Number.MIN_VALUE;
    var menor = Number.MAX_VALUE;

    let clazz = {
        encontra : function(nums) {
            let sorted = nums.sort((a, b) => {
                return a - b;
            });
            maior = sorted[sorted.length-1];
            menor = sorted[0];
        },

        pegaMenor : function() {
            return menor;
        },

        pegaMaior : function() {
            return maior;
        }
    };

    return clazz;
};