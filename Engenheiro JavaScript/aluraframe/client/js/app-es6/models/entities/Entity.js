import {EventEx} from "../../../libs-es6/events/EventEx";

export class Entity {
    constructor() {
        // JSON.stringify Converting circular structure to JSON
        //this._propertyChanged = new EventEx(this);
    }

    get propertyChanged() {
        if (!this._propertyChanged) {
            this._propertyChanged = new EventEx(this);
        }
        return this._propertyChanged;
    }

    equals(another) {
        JSON.stringify(this) == JSON.stringify(another);
    }

    static isEntity(entity) {
        return Object.getPrototypeOf(entity).constructor.__proto__.name == "Entity";
    }
}