import {Entity} from "./Entity";

export class Negociacao extends Entity {
    // SyntaxError: A class may only have one constructor
    // constructor() {
    //     this.data = new Date();
    //     this.quantidade = 1;
    //     this.valor = 0.0;
    // }

   
    constructor(data, quantidade, valor) {
        super();
        // _ é uma convenção para deixar os dados privados à classe

        // Ao invés usar a referência da data (que poderia ser alterada externamente) retorna uma nova data com base na data.
        this._data = new Date(data.getTime());
        this._quantidade = parseInt(quantidade);
        this._valor = parseFloat(valor);
        this._id = this._data.getTime();
        
        // https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze
        //Object.freeze(this);
    }

    // getVolume() {
    //     return this._quantidade * this._valor;
    // }

    // getData() {
    //     return this._data;
    // }

    // getQuantidade() {
    //     return this._quantidade;
    // }

    // getValor() {
    //     return this._valor;        
    // }

    // getVolume() {
    //     return this._quantidade * this._valor;
    // }

    get id() {
        return this._id;
    }

    get data() {
        // Ao invés de retornar a referência da data (que poderia ser alterada) retorna uma nova data com base na data.
        return new Date(this._data.getTime());
    }

    get quantidade() {
        return this._quantidade;
    }

    get valor() {
        return this._valor;        
    }

    get volume() {
        return this._quantidade * this._valor;
    }

    equals(another) {
        let isEqual = JSON.stringify(data) == JSON.stringify(data) && this._quantidade == another.quantidade &&
            this._valor == another.valor;
        return isEqual;
    }
    /**
     * Formas de uso
     * var n1 = new Negocicao(new Date(), 1, 144);
     * n1.getValor() ou usando get n1.valor;
     */

}