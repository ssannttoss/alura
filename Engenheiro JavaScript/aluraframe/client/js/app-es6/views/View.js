import {AbstractClassCannotBeInstantiatedError} from "../../libs-es6/snts";
import {AbstractNotImplemented} from "../../libs-es6/snts";

export class View {
    constructor(element) {
        AbstractClassCannotBeInstantiatedError.check(this);
        this._element = element;
    }

    _template() {
        throw new AbstractNotImplemented(this);
    }

    update(model) {
        // this._element.innerHTML = this._template(model); // vaniilla
        this._element.html(this._template(model)); // jquery
    }

    static isView(view) {
        return Object.getPrototypeOf(view).constructor.__proto__.name == "View";
    }
}