let qs = document.querySelector.bind(document);

var body = query("body");

export function query(what) {
    return qs(what);
}

export function detailEvent(e, parameters) {
    if (e.target === undefined) {
        console.log(`type: ${e.type}`);
    } else  {
        console.log(`type: ${e.type} | target: ${e.target.tagName || e.target} | innerHTML: ${e.target.innerHTML}`);
    }
}

/**
 * https://stackoverflow.com/questions/35920606/how-to-pass-intervalid-to-interval-function-in-javascript
 * // clearTimeout
 * TODO: Search for optional parameters
 * That will pass the time handle as the first argument, in front of any others you specify. 
 * It also has the benefit of supporting those follow-on arguments reliably cross-browser 
 * (some older browsers didn't support passing arguments to the callback).
 * @param {*} callback function to be called.
 * @param {*} time interval time.
 * @param {*} array of variables
 */
export function setIntervalWrapper(callback, time, parameters) {
    console.log(`setIntervalWrapper: ${parameters}`);
    var extraParameters = isDefined(parameters) ? parameters.length : 0;
    var args = Array.prototype.slice.call(arguments, extraParameters + 1);
    for(let i = 0; i < extraParameters; i++) {
        args[i+1] = parameters[i];
        console.log(`setIntervalWrapper: args[${i+1}] = ${args[i+1]}`);
    }
    args[0] = setInterval(function() {
        callback.apply(null, args);
    }, time);
}

export function isDefined(variable) {
    let result = typeof variable != "undefined";
    //console.log(`isDefined: ${variable}: ${result}`);
    return result;
}

export function scroll(offset, interval) {
    body.animate({ scrollTop: offset + "px" }, interval);
}

export function randomEx(max) {
    let random = Math.random() * max;
    return Math.floor(random);
}

export function describe(obj) {
    if (!isDefined(obj)) {
        console.log("[describe] undefined");
    } else {
        console.log(`[describe] typeof: ${typeof obj} | toString: ${Object.prototype.toString.call(obj)}`);
    }
}

export function isArray(obj) {
    describe(obj);
    if (Array.isArray) {
        return Array.isArray(obj);
    } else {
        return Object.prototype.toString.call(obj) === '[object Array]';    
    }
}

export function getStackTrace() {
    try { 
        var obj = {};
        if (isDefined(Error.captureStackTrace)) {
            Error.captureStackTrace(obj, getStackTrace);        
            return obj.stack.split("\n");
        } else {
            return ["", "","", "", ""];
        }
    } catch (ex) {
        if (!isDefined(Error.captureStackTrace)) {
            console.warn("IDE doen't not support Error.captureStackTrace");
        }
        return ["", "","", "", ""];
    }
}

export function preventDefault(e) {
    if (e) {
        e.preventDefault();
    }
}

export class NoParametersError extends Error {
     constructor(object) {
        let callerIndex = 3; // the point witch the object is instantiated.
        let method = getStackTrace()[callerIndex].replace("at","").replace(object.constructor.name + ".", "").trim();
        super(`This method '${method}' of the class ${object.constructor.name} doesn't allow arguments`);
    }

    static check() {
        if(arguments.length > 1) {
            throw new NoParametersError(arguments[0]);
        }
    }
}

export class AbstractClassCannotBeInstantiatedError extends Error {
    constructor(object) {
        let callerIndex = 4; // the point witch the object is instantiated.
        let method = getStackTrace()[callerIndex].replace("at","").replace(object.constructor.name + ".", "").trim();
        super(`Cannot create an instance of the abstract class or interface ${object.constructor.name} at ${method}`);
    }

    static check(object) {
        let callerIndex = 2; // the constructor
        let method = getStackTrace()[callerIndex].replace("at","").trim();
        if (method.startsWith("new")) {
            // if the class is extends the stack will be something like Concrete.Abstract()
            throw new AbstractClassCannotBeInstantiatedError(object);
        } 
    }
}

export class AbstractNotImplementedError extends Error {
    constructor(object) {
        let callerIndex = 1; // the method that must be implemented at derived class.
        let method = getStackTrace()[callerIndex].replace("at","");
        super(`${object.constructor.name} does not implement inherited abstract method ${method}`);
    }
}

export class StaticCannotBeInstantiatedError extends Error {
    constructor(object) {
        let callerIndex = 3; // The constructor of the statict class
        let method = getStackTrace()[callerIndex].replace("at","").replace(object.constructor.name + ".", "").trim();
        super(`Static class ${object.constructor.name} cannot be instantiated at ${method}`);
    }
}

export class InvalidTypeError extends Error {
    constructor(object, expectedType) {
        super(`Cannot convert from '${object.constructor.name}' to '${expectedType}'`);
        this._object = object;
        this._expectedType = expectedType;
        Object.freeze(this);
    }

    get object() {
        return this._object;
    }

    get expectedType() {
        return this._expectedType;
    }
}

export class DateEx {

    constructor() {
        throw new StaticCannotBeInstantiatedError(this);
    }

    static parse(dateStr) {
        let date;
        if (dateStr.includes(":")) {
        } else {
          
            // valid date format: yyyy-MM-dd -> regex: /\d{4}-\d{2}-\d{2}/            
            if (!/^\d{2}\/\d{2}\/\d{4}$/.test(dateStr)) {
                throw new Error("Date must be in format dd/mm/aaaa");
            }
            
            date = new Date(...dateStr.split('/').reverse().map((item, index) => item - index % 2));
            // // https://cursos.alura.com.br/course/javascript-es6-orientacao-a-objetos-parte-1/task/16529
            // https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Operators/Spread_operator
            // date = new Date(...
            //     dateStr.split(",").map(function (item, i) {
            //        // if (i == 1) {
            //        //     return item - 1;
            //        // } else  {
            //        //     return item;
            //        // }
            //          return item - i % 2;
            //     })
            // );

            // date = new Date(...dateStr.split(",").map((item, i) => item - i % 2);
        }
        return date;
    }

    static format(date) {
        // https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Template_literals
        let formatted = `${date.getDate()}/${(date.getMonth() + 1)}/${date.getFullYear()}`;
        return formatted;
    }
}