/**
 * Observable pattern
 * Module Pattern
 * https://scotch.io/bar-talk/4-javascript-design-patterns-you-should-know
 * https://medium.freecodecamp.com/javascript-modules-a-beginner-s-guide-783f7d7a5fcc
 */

import {Subscriber} from "./Subscriber";

export const EventFunctionType = {
   MISSING_DATA: 0,
   INVALID_DATA: 1
}

export class EventEx {
    /**
     * Do not use in class constructors
     * @param {*} owner 
     */
    constructor(owner) {
        this._subscribers = [];
        this._owner = owner;        
    }

    static checkContext(object, typeName) {
        if (object.constructor.name != typeName) {
            let context = object.constructor.name === "Subscriber" ? object.eventOwner.constructor.name : object.constructor.name
            throw new Error(`The context of the event is not the expected. Expected: ${typeName}. Context: ${context}`)
        }
    }

    /**
     * Signs and event (observable pattern).
     * @param {*} subscriber The object (context) that has the handler.
     * @param {*} handler The function that handler the event when it is fired.
     * @param {*} fireMode ES6 arrow function syntax uses “lexical scoping” while normal is dinamically
     * but this EventEx class has solved it. So, you just need to tell how the function that handlers
     * the event will be called.
     */
    sign(subscriber, handler, fireMode = EventFunctionType.NORMAL_FUNCTION) {
        let index = this._subscribers.findIndex(s => 
            s.subscriber === subscriber
        );

        if (index === -1) {
            this._subscribers.push(new Subscriber(subscriber, handler, fireMode, this._owner));
        } else {
            throw new Error("Already signed");
        }
    }

    /**
     * Unsign the event.
     * @param {*} subscriber The object (context) that has the handler. 
     */
    unsign(subscriber) {
        let result = this._subscribers.find(s => 
            s.subscriber === subscriber
        );

        if (isDefined(result)) {
            //this._subscribers.
        } else {
            throw new Error("Did not sign");
        }
    }

    /**
     * Fires the event and notify all subscribers about it.
     * @param {*} args Custom arguments to be passed to handler functions.
     */
    fire(args) {
        if (!isDefined(args)) {
            args = new EventArgs();
        }

        this._subscribers.forEach((s) => { 
            if (!args.cancel) {
                if (s._fireMode == EventFunctionType.NORMAL_FUNCTION) {
                    Reflect.apply(s.handler, s.subscriber, [this._owner, args])   
                } else if (s._fireMode == EventFunctionType.ARROW_FUNCTION) {
                    s.handler(this._owner, args); 
                }
            }
        });
    }
}
