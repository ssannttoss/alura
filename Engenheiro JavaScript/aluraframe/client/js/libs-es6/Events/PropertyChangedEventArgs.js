import {EventArgs} from "./EventArgs";

export class PropertyChangedEventArgs extends EventArgs {
    constructor(propertyName) {
        super();
        this._propertyName = propertyName;
        // let callerIndex = 2; // the point witch the object is instantiated.
        // let regEx = new RegExp(".*at.[as*");
        // let method = getStackTrace()[callerIndex].replace(//,"").replace("/[.*","").trim(); 
        // console.log(method);
    }

    get propertyName() {
        return this._propertyName;
    }

    toString() {
        return this._propertyName;
    }
}