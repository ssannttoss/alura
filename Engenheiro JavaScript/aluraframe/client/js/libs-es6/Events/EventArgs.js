export class EventArgs {
    constructor(){
        this._cancel = false;
    }

    get cancel() {
        return this._cancel;
    }

    set cancel(value) {
        this._cancel = value;
    }
}