export class Subscriber {
    constructor(subscriber, handler, fireMode, eventOwner) {
        this._subscriber = subscriber;
        this._handler = handler;
        this._fireMode = fireMode;
        this._eventOwner = eventOwner;
    }

    get subscriber() {
        return this._subscriber;
    }

    get handler() {
        return this._handler;
    }

    get fireMode() {
        return this.fireMode;
    }

    get eventOwner() {
        return this._eventOwner;
    }
}