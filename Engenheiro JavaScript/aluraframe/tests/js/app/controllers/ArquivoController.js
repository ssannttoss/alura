class ArquivoController {

    constructor() {
        this._inputDados = document.querySelector('.dados-arquivo');
    }

    envia() {
        //cria um Arquivo com as suas propriedades;
        let arquivo = this._criarArquivo();        
        this._limpaFormulario();
        console.log(arquivo.toString());
    }

    _criarArquivo() {
        let input = this._inputDados.value.split("/");
        // spread operator
        let arquivo = new Arquivo(...
            input.map((item) => item.toUpperCase())
        );
        return arquivo;
    }

    _limpaFormulario() {
        this._inputDados.value = '';
        this._inputDados.focus();
    }
}