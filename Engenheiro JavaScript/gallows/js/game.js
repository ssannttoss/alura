var createGame = function(sprite) {
    let stage = 1;
    let secreteWord = '';
    let letterSlots = [];    

    var createSlots = () => {
        letterSlots = [secreteWord.length];

        for(let i = 0; i < secreteWord.length; i++) {
            letterSlots[i] = '';
        }

        /* other option
        
        //using push
        for(var i = 0; i < palavraSecreta.length; i++) {
            letterPlaces.push('');
        }

        //using Array and fill
        letterPlaces = Array(palavraSecreta.length).fill('');

        */
    }

    var nextStage = () => stage = 2;
    
    var getStage = () => stage;

    var setSecrete = secrete => {
        if (!secrete || secrete.trim().length == 0) {
            throw new Error('Invalid secrete');
        }

        secreteWord = secrete;
        createSlots();
        nextStage();        
    }

    var getSlots = () => letterSlots;

    var findLetter = letter => {
        let index = secreteWord.indexOf(letter);
        let hasLetter = index >= 0;

        while (index >= 0) {
            fillSlots(letter, index);
            index = secreteWord.indexOf(letter, index + 1);            
        } 

        return hasLetter;
    }

    var fillSlots = (letter, position) => {
        letterSlots[position] = letter;
    }

    var guess = letter => {
        if (!letter || letter.trim().length == 0) {
            throw new Error('Invalid guess');
        }

        let exp = new RegExp(letter, 'gi');
        let result;
        let success = false;
        
        while(result = exp.exec(secreteWord)) {
            success = true;
            letterSlots[result.index] = letter;
        }

        if (!success) {
            sprite.nextFrame();
        }        

        /*if (findLetter(letter)) {
            console.log(letterSlots);    
        } else if (!sprite.isFinished()) {
            console.log('nextFrame');
            sprite.nextFrame();
        }*/
    }

    var won = () => secreteWord !== '' && letterSlots.indexOf('') === -1 && !sprite.isFinished();

    var lose = () => sprite.isFinished();

    var wonOrLose = () => won() || lose();    

    var restart = function() {
        sprite.reset();
        secreteWord = '';
        letterSlots = [];
        stage = 1;
    }

    return {
        setSecrete,
        getSlots,
        getStage,
        nextStage,
        guess,
        won,
        lose,
        wonOrLose,
        restart
    }
}