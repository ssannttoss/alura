var createController = function(game) {
    let $input = $('#input');
    let $slots = $('.slots');

    var showSlots = () => {
        $slots.empty();
        game.getSlots().forEach(function(slot) {
            $('<li>')
                .addClass('slot')
                .text(slot)
                .appendTo($slots);
        });
    }

    var changePlaceHolder = text => $input.attr('placeholder',text);        

    var setSecreteWord = () => {
        game.setSecrete($input.val().trim());    
        $input.val('');
    }

    var readGuess = () => {
        game.guess($input.val().trim().substr(0,1));
        $input.val('');
        showSlots();

        if (game.wonOrLose()) {
            setTimeout(function() {
                if (game.won()) {     
                    alert('You won');                
                } else {
                    alert('You lose');                
                }
            }, 200);
            
            restart();  
        }
    }

    var restart = () => {
        game.restart();
        changePlaceHolder('secret word');     
    }

    var start = () => {
        $input.keypress(e => {
            try {
                if (e.which == 13) {
                    switch(game.getStage()) {
                        case 1:
                            setSecreteWord();
                            changePlaceHolder('guess');
                            showSlots();                        
                            break;
                        case 2:            
                            readGuess();
                            break;
                    }
                }
            } catch(error) {
                alert(error);
            }
        });        
    }
    return {
        start: start
    }
}

