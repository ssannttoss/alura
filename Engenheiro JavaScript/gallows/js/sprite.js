var createSprite = function(selector) {
    const $el = $(selector);

    var frames = [
        'frame1', 'frame2', 'frame3', 'frame4',
        'frame5', 'frame6', 'frame7', 'frame8', 
        'frame9'
    ];

    let last = frames.length - 1;
    let current = 0;

    $el.addClass(frames[current]);

    var moveFrame = (from, to) => {
        $el.removeClass(from)
            .addClass(to);
    };

    var nextFrame = () => {
        if (hasNext()) {
            moveFrame(frames[current], frames[++current]);
        }
    };
    
    var hasNext = () => current + 1 <= last;

    var reset = () => {
        var old = current;
        current = -1;
        moveFrame(frames[old], frames[++current]);
    }

    var isFinished = () => !hasNext();

    return {
        nextFrame,
        isFinished,
        reset
    };
};