export const httpResultHandler = (res) => 
    res.ok ? res.json() : Promise.reject(res.statusText);

export const log = param => {
    console.log(param);
    return param
};


export const timeoutPromise = (milliseconds, promise) => {
    const timeout =  new Promise((resolve, reject) =>
        setTimeout(() => 
            reject(`Limite da operação excedido (limite: ${milliseconds} ms)`), 
                milliseconds));

    return Promise.race([
        timeout, 
        promise
    ]);
};

/**
 * Received a data from a previous promise, waits the specified milliseconds
 * and then pass the data of previous promise to the next one. Works like a pipe
 * between promises's 'then'. 
 *
 * @param {number} milliseconds - interval between the next promise call
 */
export const delay = milliseconds => data =>
    new Promise((resolve, reject) => 
        setTimeout(() => resolve(data), milliseconds)
    );

/**
 * /** 
 * Retries a promise execution until success limited by retries
 * with a delay between retries
 * 
 * @param {number} retries - number of times a promise will be called until success
 * @param {number} millisecond - interval between retries
 * @param {function} fn - returns a promise to be called. Once the same promise cannot be reused, we
 * need to return a function that return a new promise
 */
export const retry = (retries, millisecond, fn) =>
    fn().catch(err => {
        console.log(`Promise failed. Remaining retries: ${retries}`); 
        const fnPromiseWithDelay = delay(millisecond);
        return fnPromiseWithDelay() //invoke the promise
            .then(() =>
                retries > 1
                ? retry(--retries, millisecond, fn) // another attempt to resolve the promise
                : Promise.reject(err) // no more attempts, so return error
            ); 
    });
    
