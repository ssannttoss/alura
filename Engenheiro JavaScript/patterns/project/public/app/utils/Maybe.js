/**
 *  const result = Maybe.of(10)
        .map(value => value + 10) 
        .map(value => value + 30)
        .get(); // returns 50
 */

 /** Monada */
export class Maybe {
    constructor(value) {
        this._value = value;
    }

    static of(value) {
        return new Maybe(value);
    }

    isNothing() {
        return this._value === null || this._value === undefined;
    }

    map(fn) {
        if (this.isNothing()) return Maybe.of(null);
        return Maybe.of(fn(this._value));
    }

    getOrElse(defaultValue) {
        if (this.isNothing()) return defaultValue;
        return this._value
    }
}