import { log, timeoutPromise, retry } from './utils/promise-helper.js';
import './utils/array-helpers.js';
import { notaFiscalService as service } from './notaFiscal/service.js';
import { takeUntil, debounceTimes, partialize, pipe } from './utils/operators.js';
import { EventEmitter } from './utils/event-emitter.js';

import { Maybe } from './utils/maybe.js';

const codigoFiltro = '2143';

const getNotasFiscais = () => service.sumItems(codigoFiltro);

const operations = pipe(
    // once the functions are returns callbacks
    // the order in the pipe must be inverted    
    partialize(takeUntil, 3),
    partialize(debounceTimes, 500)
);

const action = operations(() => {
    retry(3, 3000, () => timeoutPromise(200, getNotasFiscais()))
    .then(total => EventEmitter.emit('itensTotalizados', total))
    .catch(console.log);
});

document
    .querySelector('#myButton')
    .onclick = action;

