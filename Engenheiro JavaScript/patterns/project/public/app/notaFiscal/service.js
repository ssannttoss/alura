import { httpResultHandler } from '../utils/promise-helper.js';
import { partialize, pipe } from '../utils/operators.js';
import { Maybe } from '../utils/Maybe.js';

const API = 'http://localhost:3000/notas';

const getItemsFromNotas = notasFiscaisM => 
    notasFiscaisM.map(notaFiscal => notaFiscal.$flatMap(notaFiscal => notaFiscal.itens));

const filterItemsByCode = (code, itemsM) => itemsM.map(items => items.filter(item => item.codigo == code));

const sumItemsValue = itemsM => itemsM.map(items => items.reduce((total, item) => total + item.valor, 0));

export const notaFiscalService = {
    listAll() {
        return fetch(API)
        .then(httpResultHandler)
        .then(notasFiscais => Maybe.of(notasFiscais))
        .catch(err => {
            console.log(err);
            return Promise.reject('Não foi possível obter as notas fiscais');
        });
    },

    sumItems(code) {
        // Partial Application
        // https://msdn.microsoft.com/en-us/magazine/gg575560.aspx?f=255&MSPPError=-2147217396
        // http://benalman.com/news/2012/09/partial-application-in-javascript/
        const filterItems = partialize(filterItemsByCode, code);

        // Point Free Functions
        // const sumItems = compose(sumItemsValue, filterItems, getItemsFromNotas);
        const sumItems = pipe(getItemsFromNotas, filterItems, sumItemsValue);

        // composing
        return this.listAll()
            .then(sumItems)
            .then(result => result.getOrElse(0));
    }
};