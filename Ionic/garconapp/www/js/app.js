$('.collection-item').on('click', function() {
    let $badge = $('.badge', this);

    if ($badge.length == 0) {
        $badge = $('<span class="badge brown-text">1</span>');
        $(this).append($badge);
    } else {
        let valor = parseInt($badge.text());
        $badge.text(valor + 1);
    }
});

$('#btn-confirmacao').on('click', function() {
    let resumo = [];
    $('.badge').parent().each(function() {
        var produto = this.firstChild.textContent;
        var quantidade = this.lastChild.textContent;
        resumo.push(`${produto}: ${quantidade}`);
    });
    $('#resumo').text(resumo.join(',  '));
});

$(document).ready(function() {
    initModal();

    $('.collapsible').collapsible();

    $(".button-collapse").sideNav({
        menuWidth: 300, //default
        edge: 'left', //choose the horizontal origin
        closeOnClick: true, //closes side-nav on <a> clicks, useful for Angular/Meteor
        draggable: true, // choose whether you can drag to open on touch screens
        onOpen: function(e1) {
        },
        onClose: function(e2) {
        }
    });

    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false, // Does not change width of dropdown to that of the activator
        hover: false, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: true, // Displays dropdown below the button
        alignment: 'left', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
    });

    $('.side-nav').mousedown(function() {
        $('.button-collapse').sideNav('hide');
    });
});

var initModal = function() {
    $('#modal-confirmacao').modal({
        dismissible: true, //Modal can not be dismissed by clicking outside of the modal
        opacity: .5,
        inDuration: 300, //transition 'in' duration
        outDuration: 200, //transition 'out' duration
        startingTop: '4%', //starting top style attribute
        endingTop: '10%', //ending top style attribute
        ready: function(modal, trigger) {
            $("#btn-confirmacao").addClass("scale-transition scale-out");
            Materialize.Toast.removeAll();
            if ($('#numero-mesa').val() != "") {
                $('#numero-mesa').focus(); //bug fix
                $('#btn-confirmar-pedido').focus();
            } else {
                Materialize.updateTextFields();
                $('#btn-confirmar-pedido').focus();
            }
        },
        complete: function() {
            $("#btn-confirmacao").removeClass("scale-out");
        }
    });
};

$('.collection-item').on('click', '.badge', function() {
    let valor = parseInt($(this).text());

    if (valor > 1) {
        $(this).text(valor - 1);
    } else {
        $(this).remove();
    }
    return false;
});

$('.acao-limpar').on('click', function() {
    limparPedido();
    var $toast = $(`<span>Pedido descartado</span>`);
    Materialize.toast($toast, 1000, 'red');
});

function limparPedido() {
    $('#numero-mesa').val('');
    $('.badge').remove();
}

function fecharPedido() {
    $('#modal-confirmacao').modal('open');
};

$('#scan-qrcode').click(function scan() {
    cordova.plugins.barcodeScanner.scan(
        function(result) {
            let numeroMesa;
            try {
                numeroMesa = parseInt(result.text);
            } catch(err) {
                var $toast = $(`<span><b>Falha ao obter número da mesa.<br>Erro: ${err}</b></span>`);
                Materialize.toast($toast, 3000, 'red white-text');
                return;
            }

            if (Number.isNaN(numeroMesa)) {
                return;
            }

            $('#numero-mesa').val(numeroMesa);
            var $toast = $(`<span>Mesa ${numeroMesa}</span>`)
                        .add($('<button id="btn-fechar-pedido" class="btn-flat toast-action white-text" onclick="fecharPedido()">Visualizar pedido</button>'));
            Materialize.toast($toast, 5000, 'yellow darken-4');
        },
        function(err) {
            $('#numero-mesa').val('');
            var $toast = $(`<span><b>Falha ao obter número da mesa.<br>Erro: ${err}</b></span>`);
            Materialize.toast($toast, 3000, 'red white-text')
        }, {
            // preferences
            preferFrontCamera: false, //iOS and Android
            showFlipCameraButton: false, //iOS and Android
            showTorchButton: true, //iOS and Android
            torchOn: false, //Android, launch with the torch switched on (if available)
            saveHistory: false, // Android, save scan history (default false)
            prompt: "Posicione o código da mesa na área", // Android
            resultDisplayDuration: 0, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
            formats: "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
            orientation : "portrait", // Android only (portrait|landscape), default unset so it rotates with the device
            //disableAnimations : true, // iOS
            disableSuccessBeep: false // iOS and Android
        }
    );
});

$('#btn-fechar-pedido').click(function() {
    let mesa = $('#numero-mesa').val();
    let pedidos = $('#resumo').text();

    if (!mesa || !pedidos) {
        var $toast = $(`<span><b>Falha enviar pedido.<br>Erro: Mesa ou pedidos não informados</b></span>`);
        Materialize.toast($toast, 3000, 'red white-text')
    } else {
        $.ajax({
            url: 'http://cozinhapp.sergiolopes.org/novo-pedido',
            data: {
                mesa:mesa,
                pedido: pedidos
            },
            error: function(err) {
                console.error('Falha enviar pedido', err.responseText);
                var $toast = $(`<span><b>Falha enviar pedido.<br>Erro: ${err.responseText}</b></span>`);
                Materialize.toast($toast, 3000, 'red white-text');
            },
            success: function(data) {
                navigator.vibrate([500, 500, 1000]); //vibrate for 1s, wait 1s then vibrate again for 3s
                var $toast = $(`<span><b>Pedido enviado pra cozinha</b></span>`);
                Materialize.toast($toast, 3000, 'green');
                limparPedido();
                console.log(data);
            }
        });
    }
});