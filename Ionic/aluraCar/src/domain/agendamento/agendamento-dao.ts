import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { Agendamento } from "./agendamento";


@Injectable()
export class AgendamentoDao {
    constructor(private _storage: Storage) {

    }

    private _getKey(agendamento: Agendamento) {
        return agendamento.email + agendamento.data.substr(0, 10);
    }

    public salva(agendamento: Agendamento): Promise<any> {
        const key = this._getKey(agendamento);
        const promise = this._storage.set(key, agendamento);
        return promise;
    }

    public busca(agendamento: Agendamento): Promise<Agendamento> {
        const key = this._getKey(agendamento);
        const promise = this._storage.get(key);
        return promise;
    }
}