import { Http } from '@angular/http';
import { Agendamento } from './agendamento';
import { Injectable } from '@angular/core';
import { AgendamentoDao } from './agendamento-dao';

@Injectable()
export class AgendamentoService {
    constructor(
        private _http: Http,
        private _dao: AgendamentoDao
    ){}

    public agenda(agendamento: Agendamento) {
        let api = `https://aluracar.herokuapp.com/salvarpedido?carro=${agendamento.carro.nome}&nome=${agendamento.nome}&preco=${agendamento.valor}&endereco=${agendamento.endereco}&email=${agendamento.email}&dataAgendamento=${agendamento.data}`;

        return this._dao.busca(agendamento)
            .then((agendamentoEncontrado) => {
                if (agendamentoEncontrado) throw new Error('Este agendamento já foi realizado. Verifique');
                return this._http
                        .get(api)
                        .toPromise()
                        .then(() => {
                            agendamento.confirmado = true
                        }, (err) => {
                            console.error(err);
                        })
                        .then(() => {
                            return this._dao.salva(agendamento);
                        })
                        .then(() => agendamento.confirmado);
                        });
    }
}