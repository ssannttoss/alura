import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { EscolhaPage } from './../escolha/escolha';
import { Carro } from '../../domain/carro/carro';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  private _carros: Array<Carro>;
  constructor(
    public navCtrl: NavController,
    private _http: Http,
    private _loadingCtrl: LoadingController,
    private _alert: AlertController) {
  }

  public get carros():  Array<Carro> {
    return this._carros;
  }

  public ngOnInit(): void {
    const loader = this._loadingCtrl.create({
      content: 'Buscando carros. Aguarde...'
    });

    loader.present({
      keyboardClose: true
    });
    this._http
      .get('https://aluracar.herokuapp.com')
      .map((res) => { return res.json()})
      .toPromise()
      .then((dados) => {
        this._carros = dados;
        loader.dismiss();
      })
      .catch(err => {
        console.log(err);
        loader.dismiss();
        this._alert.create({
          buttons: [{ text: 'Ok, entendi' }],
          title: 'Falha na conexão',
          subTitle: 'Não foi possível obter os carros. Tente novamente mais tarde'
        }).present();
      });
  }

  public seleciona(carro: Carro) {
    this.navCtrl.push(EscolhaPage, { carroSelecionado: carro});
  }
}
