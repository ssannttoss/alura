import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Carro } from '../../domain/carro/carro';
import { Alert } from 'ionic-angular/components/alert/alert';
import { Agendamento } from '../../domain/agendamento/agendamento';
import { HomePage } from '../home/home';
import { AgendamentoService } from '../../domain/agendamento/agendamento-service';

@Component({
  templateUrl: 'cadastro.html'
})
export class CadastroPage {

  private _carro: Carro;
  private _precoTotal: number;
  private _agendamento: Agendamento;
  private _alert: Alert;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _agendamentoService: AgendamentoService,
    private _alertCtrl: AlertController
  ) {
    this._carro = this.navParams.get('carro');
    this._precoTotal = this.navParams.get('precoTotal');
    this._agendamento = new Agendamento(this._carro, this._precoTotal);
    this._alert = this._alertCtrl.create({
      title: 'Aviso',
      buttons: [{text: 'Ok', handler: () => this.navCtrl.setRoot(HomePage)}],
      enableBackdropDismiss: true
    });
  }

  public get carro(): Carro {
    return this._carro;
  }

  public get precoTotal(): number {
    return this._precoTotal;
  }

  public get agendamento(): Agendamento {
    return this._agendamento;
  }

  public agenda(): void {
    if (!this.agendamento.nome || !this.agendamento.endereco || !this.agendamento.email || !this.agendamento.data) {
      this._alertCtrl.create({
        title: 'Preenchimento obrigatório',
        subTitle: 'Você deve reencher todas as informações',
        buttons: [{ text: 'Ok'}]
      }).present();
      return;
    }

    this._agendamentoService.agenda(this.agendamento)
      .then((confirmado: boolean) => {
        confirmado ?
          this._alert.setSubTitle('Agendamento realizado com sucesso.'):
          this._alert.setSubTitle('Não foi possível realizar o agendamento. Tente mais tarde.');
        this._alert.present();
      })
      .catch((err: Error) => {
        this._alert.setTitle('Erro no agendamento');
        this._alert.setSubTitle(err.message);
        this._alert.present();
      });
  }
}
