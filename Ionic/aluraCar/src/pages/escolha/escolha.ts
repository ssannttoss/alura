import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Acessorio } from '../../domain/carro/acessorio';
import { Carro } from '../../domain/carro/carro';
import { CadastroPage } from '../cadastro/cadastro';

@Component({
    templateUrl: 'escolha.html'
})
export class EscolhaPage {
    private _carro: Carro;
    private _acessorios: Array<Acessorio>;
    private _precoTotal: number;

    constructor(public navCtrl: NavController, public navParams: NavParams) {
        this._carro = navParams.get('carroSelecionado')
        this._acessorios = [
            new Acessorio('Freio ABS', 800),
            new Acessorio('Ar-condicionado', 1000),
            new Acessorio('MP3 Player', 500)
        ];
        this._precoTotal = this._carro.preco;
    }

    public get carro(): {} {
        return this._carro;
    }

    public get acessorios(): Array<{}> {
        return this._acessorios;
    }

    public get precoTotal(): number {
        return this._precoTotal;
    }

    public atualizaTotal(ligado: boolean, acessorio): void {
        ligado?
            this._precoTotal += acessorio.preco:
            this._precoTotal -= acessorio.preco;
    }

    public avancaNoAgendamento(): void {
        this.navCtrl.push(CadastroPage, {
            carro: this.carro,
            precoTotal: this._precoTotal
        })
    }
}