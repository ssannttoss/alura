var app = require("./config/express")();

// Configurando o socket io no node.s
var http = require("http").Server(app); // passando para o Servidor Http do Node a função do express
var io = require("socket.io")(http);

app.set("io", io);

var porta = process.env.PORT || 3000;
var server = http.listen(porta, () => {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Server running at http://%s:%s', host, port);
})