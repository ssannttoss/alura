var express = require("express");
var expressValidator = require("express-validator");
var load = require("express-load");
var bodyParser = require("body-parser");

module.exports = () => {
    var app = express();

    // express permite a configuração de uma função  que trata dos recursos que são estáticos.
    // utilizamos o app.use() e passamos para ele express.static e o caminho './app/public'. 
    // Temos o seguinte código:
    app.use(express.static("./app/public"));

    app.set("view engine", "ejs"); // register the template engine
    app.set("views", "./app/views"); // specify the views directory

    // use recebe funções que serão aplicadas ao request na ordem que definirmos. 
    // Esse tipo de situação é conhecida como middleware.
    app.use(bodyParser.urlencoded({ extended: true })); // Extended é necessário para interpretar formulários complexos
    app.use(bodyParser.json());
    app.use(expressValidator()); // chamado após o processamento do body



    // invoking load
    load("infra", {cwd: "app"})
        .then("routes")
        .into(app);

    // adicionado um custom middleware
    // precisa ser chamado após a configuração das rotas
    app.use(function(req, res, next) {
        res.status(404).render("errors/404", { path: req.path });
        next();
    });

    // Middleware com funções de 4 argumentos são chamadas prioritarimente
    // pois assume-se que o primeiro argumento é o erro.
    app.use(function(err, req, res, next) {
        if (app.get("env") == 'production') {
            res.status(500).render("errors/500");
            return;
        } else {
            res.json(err);
            next();
        }
    });

    console.log(`app.get("env"): ${app.get("env")}`); // CMD: set NODE_ENV
    return app;
}