var http = require("http");
let configuracoes = {
    hostname: "localhost",
    port: 3000,
    path: "/produtos",
    method: "post",
    headers: {
        "Accept":"application/json",
        "Content-type":"application/json"
        // "Accept":"text/html"

    }
};

// cria a requisição
var clientRequest = http.request(configuracoes, function(res) {
    console.log(res.statusCode);
    res.on("data", function(body) {
        console.log(`Body: ${body}`);
    })
});

var produto = {
    titulo: "mais sobre nome",
    descricao: "node javascript e um pouco sobre http",
    preco: 100
}
// dispara a requisição
clientRequest.end(JSON.stringify(produto));