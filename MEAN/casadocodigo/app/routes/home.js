module.exports = function(app) {
    app.get("/", function(req, res) {
         let connection = app.infra.connectionFactory();

        //new utilizado para contextualizar o this dentro da função.
        let produtosDAO = new app.infra.ProdutosDAO(connection); 

        try {
            produtosDAO.lista(function(err, results) {
                if (err) {
                    return next(err);
                } else {
                    res.format({
                        html: function() {
                            res.render("home/index", { livros: results });
                        },
                        json: function() {
                            res.json(results);
                        }
                    })
                }
            });
        } finally {
            if (connection) {
                connection.end();
            }   
        }
    });
}