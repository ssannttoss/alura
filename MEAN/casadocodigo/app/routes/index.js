// routes.js
module.exports = (app) => { 
    app.get("/produtos", (req, res, next) => {
        let connection = app.infra.connectionFactory();

        //new utilizado para contextualizar o this dentro da função.
        let produtosDAO = new app.infra.ProdutosDAO(connection); 

        try {
            produtosDAO.lista(function(err, results) {
                if (err) {
                    return next(err);
                } else {
                    // res.render("produtos/lista", { 
                    //     lista: results 
                    // });
                    res.format({
                        html: function() {
                            res.render("produtos/lista", { lista: results });
                        },
                        json: function() {
                            res.json(results);
                        }
                    })
                }
            });
        } finally {
            if (connection) {
                connection.end();
            }   
        }
    });
    
    app.get("/produtos/form",function(req, res) {
        res.render("produtos/form", { errosValidacao: {}, produto: {} });
    });

    /**
     * function
     * req -> requisição
     * res -> resposta
     * next -> 
     */
    app.post("/produtos", function(req, res, next) {
        let produto = req.body;
        req.assert("titulo", "Título é obrigatório").notEmpty();
        req.assert("preco", "Formato inválido").isFloat();

        let err = req.validationErrors();

        if (err) {
            res.format( {
                html: () => res.status(400).render("produtos/form", { errosValidacao: err, produto: produto }),
                json: () => res.status(400).json(err)
            });
            return;
        }

        console.log(produto);
        let connection = app.infra.connectionFactory();

        try {
        let produtosDAO = new app.infra.ProdutosDAO(connection);
            produtosDAO.salva(produto, function(err, results) {
                if (err) {
                    return next(err);
                } else {
                    // pattern always redirect after post
                    res.redirect("/produtos");
                }
            });
        } finally {
            if (connection) {
                 connection.end();
            }
        }
    });
}