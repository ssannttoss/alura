module.exports = function(app) {
    app.get("/promocoes/form", function(req, res) {
        let connection = app.infra.connectionFactory();

        //new utilizado para contextualizar o this dentro da função.
        let produtosDAO = new app.infra.ProdutosDAO(connection); 

        try {
            produtosDAO.lista(function(err, results) {
                if (err) {
                    return next(err);
                } else {
                    // res.render("produtos/lista", { 
                    //     lista: results 
                    // });
                    res.format({
                        html: function() {
                            res.render("promocoes/form", { lista: results });
                        }
                    })
                }
            });
        } finally {
            if (connection) {
                connection.end();
            }   
        }
    });

    app.post("/promocoes", function(req, res) {
        var promocao = req.body;
        console.info(promocao);
        var io = app.get("io");
        io.emit("novaPromocao", promocao);
        res.redirect("/promocoes/form");
    });
}