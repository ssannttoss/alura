var mysql = require("mysql");

function createDBConnection() {
    let database = "casadocodigo_nodejs";
    if (process.env.NODE_ENV == 'test') {
        database = "casadocodigo_nodejs_test";
    }

    let connection = mysql.createConnection({
            host: "localhost",
            user: "root",
            password: "",
            database: database
    });

    return connection;    
}

// wrapper para prevenir que o auto-load já invoke a função e assim crie automaticamente uma conexão.
module.exports = function() {
    return createDBConnection;
}