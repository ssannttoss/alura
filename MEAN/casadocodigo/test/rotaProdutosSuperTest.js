/**
 * o Supertest, que integra com o Mocha e ajuda a implementar o código do teste, deixando-o mais limpo mais claro.
 * ganhamos também a possibilidade de executar o teste sem precisar que a aplicação a ser testada esteja rodando, 
 * pois o Supertest consegue se integrar facilmente com o Express e assim é possível testar diretamente a rota.
 */

var express = require("../config/express")();
var http = require("supertest")(express);
describe("#produtosControllerSuperTest", function() {
    var limpaTable = function(done) {
        var connection = express.infra.connectionFactory();
        connection.query("delete from produtos where 1 = 1", function(err, result) {
            if (!err) {
                done();
            }
        });
    };

    beforeEach(limpaTable);
    afterEach(limpaTable);

    it ("#listagemJsonTest", function(done) {
        http.get("/produtos")
            .set("Accept", "application/json")
            .expect(200) //status
            .expect("Content-type", /json/, done);
    });

    it ("#cadastroInvalidoTest", function(done) {
        http.post("/produtos")
            .send({ titulo:"", descricao:"novo livro" })
            .expect(400, done); //status
    });

    it ("#cadastroValidoTest", function(done) {
        http.post("/produtos")
            .send({ titulo:"titulo", descricao:"novo livro", preco:20.50 })
            .expect(302, done); //status
    });
});