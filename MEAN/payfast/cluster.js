var cluster = require("cluster");
var os = require("os");
const cpus = os.cpus();

if (cluster.isMaster) {
    console.info("Master executing.");
    cpus.forEach(() => {
        cluster.fork();
    });

    cluster.on("listening", worker => {
        console.info("Cluster %d connected.", worker.process.pid);
    });

    cluster.on("disconnected", worker => {
        console.warn("Cluster %d desconnected.", worker.process.id);
    });

    cluster.on("exit", worker => {
        console.warn("Cluster %d has exited", worker.process.pid);
        cluster.fork();
    });
} else {
    console.info("Slave executing.");
    require("./index.js");
}