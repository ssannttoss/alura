module.exports = function (app) {
    var cartoesCliente = new app.servicos.CartoesClient();
    cartoesCliente.autoriza({}, function (err, request, response, result) {
        if (err) {
            console.error("Falhou");
        } else {
            console.log("Sucesso");
        }
    });
}

/*
curl -X POST -v -H "Content-type: application/json"
        http://localhost:3000/pagamentos/pagamento
        -d '{
        "pagamento": {
          "forma_de_pagamento": "cartao",
          "valor": "10.87",
          "moeda": "BRL",
          "descricao": "descrição do pagamento"
        },

        "cartao": {
          "numero": "1234567890123456",
          "bandeira": "VISA",
          "ano_de_expiracao": "2020",
          "mes_de_expiracao": "12",
          "cvv": "12"
        }
      }' ; echo
*/

/*
curl -X POST -v -H "Content-type: application/json"
          http://localhost:3000/correios/calculo-prazo
          -d '{
          "nCdServico": "40010",
          "sCepOrigem": "05303030",
          "sCepDestino":"65066635"
      }'
*/

/*
curl -X POST http://192.168.1.4:3000/upload/imagem -v
        -H "filename: <nome_do_arquivo>"
        -H "Content-Type: application/octet-stream"
        --data-binary @imagem.jpg
*/