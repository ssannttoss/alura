var fs = require("fs");
module.exports = function(app) {
    app.post("/upload/image", function(req, res) {
        let file = req.headers.filename;
        console.info(`File '${file}' has been received`);
        req.pipe(fs.createWriteStream(`files/${file}`));
        res.status(201).send("ok");
    });
}