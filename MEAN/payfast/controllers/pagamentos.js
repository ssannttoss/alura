const PAGAMENTO_CRIADO = "CRIADO";
const PAGAMENTO_CONFIRMADO = "CONFIRMADO";
const PAGAMENTO_CANCELADO = "CANCELADO";

var logger = require('../servicos/logger.js');

module.exports = function(app) {

    app.get("/pagamentos", function(req, res) {
        logger.info(req.url);
        res.send("ok");
    });

    app.get('/pagamentos/pagamento/:id', function(req, res) {
        let id = req.params.id;

        let connection = app.persistencia.connectionFactory();
        let pagamentoDao = new app.persistencia.PagamentoDao(connection);
        pagamentoDao.buscaPorId(id, function(err, result) {
            if (err || result.lenth == 0) {
                res.status(404).send(`Nenhum pagamento encontrado com o id ${id}`);
            } else {
                res.status(200).send(JSON.stringify(result[0]));
                logger.info(result[0]);
            }
        });
    });

    app.delete('/pagamentos/pagamento/:id', function(req, res) {
        let id = req.params.id;
        let pagamento = {};

        pagamento.id = id;
        pagamento.status = PAGAMENTO_CANCELADO;

        let connection = app.persistencia.connectionFactory();
        let pagamentoDao = new app.persistencia.PagamentoDao(connection);

        pagamentoDao.atualiza(pagamento, function(err, result) {
            if (err) {
                logger.error(err);
                res.status(500).send(err);
            } else if (result.affectedRows == 0) {
                res.status(400).send(`Pagamento ${id} não encontrado.`);
            } else {
                logger.info(`Pagamento ${id} deletado.`);
                res.location(req.url);
                var response = {
                    dados_do_pagamento: pagamento,
                    links: [
                    ]
                }

                res.status(204).send(pagamento);
            }
        })
    });

    app.put('/pagamentos/pagamento/:id', function(req, res) {
        let id = req.params.id;
        let pagamento = {};

        pagamento.id = id;
        pagamento.status = PAGAMENTO_CONFIRMADO;

        let connection = app.persistencia.connectionFactory();
        let pagamentoDao = new app.persistencia.PagamentoDao(connection, app);

        pagamentoDao.atualiza(pagamento, function(err, result) {
            if (err) {
                logger.error(err);
                res.status(500).send(err);
            } else if (result.affectedRows == 0) {
                res.status(400).send(`Pagamento ${id} não encontrado.`);
            } else {
                logger.info(`Pagamento ${id} confirmado.`);
                res.location(req.url);
                var response = {
                    dados_do_pagamento: pagamento,

                    links: [
                        {
                            href: res.get("location"),
                            rel: "self",
                            method: "GET"
                        }
                    ]
                }

                res.status(200).json(response).send();
            }
        })

    });

    app.post("/pagamentos/pagamento", function(req, res) {
        let pagamento = req.body["pagamento"];
        logger.info(`Processando pagamento ${JSON.stringify(pagamento)}...`);

        // função max é custom. Ver custom-exress.js
        req.checkBody("pagamento.forma_de_pagamento", "Forma de pagamento não informada.").notEmpty().max(255);
        req.checkBody("pagamento.moeda", "Moeda de pagamento não informada ou inválida.").notEmpty().len(3, 3);
        req.checkBody("pagamento.descricao", "Descrição do pagamento não informada ou inválida.").notEmpty().max(1024);
        req.checkBody("pagamento.valor", "Valor do pagamento não informado ou inválido.").notEmpty().isFloat();

        req.getValidationResult().then((result) => {
            if (!result.isEmpty()) {
                logger.error(result.array());
                // res.status(400).send('A requisição não é válida: ' +
                //     result.array().map(err => err.msg).join(';'));
                res.status(400).send(result.array());
                return;
            } else {
                if (pagamento.forma_de_pagamento == "cartao") {
                    let cartao = req.body["cartao"];
                    logger.info(`Processando pagamento por cartão ${JSON.stringify(cartao)}...`);

                    let cartoesClient = new app.servicos.CartoesClient();
                    cartoesClient.autoriza(cartao, function(errCartao, reqCartao, resCartao, resultCartao) {
                        if (errCartao) {
                            logger.error(errCartao);
                            res.status(400).send('Não foi possível registrar o pagamento');
                        } else {
                            res.location(`${req.url}/${pagamento.id}`);
                            var response = {
                                dados_do_pagamento: pagamento,
                                cartao: resultCartao,

                                links: [
                                    {
                                        href: res.get("location"),
                                        rel: "confirmar",
                                        method: "PUT"
                                    },
                                    {
                                        href: res.get("location"),
                                        rel: "cancelar",
                                        method: "DELETE"
                                    },
                                ]
                            }

                            res.status(201).json(response);
                        }
                    });

                } else {

                    pagamento.status = PAGAMENTO_CRIADO;
                    pagamento.data = new Date();

                    let connection = app.persistencia.connectionFactory();
                    let pagamentoDao = new app.persistencia.PagamentoDao(connection);

                    pagamentoDao.salva(pagamento, function(err, result) {
                        if (err) {
                            res.status(500).send('Não foi possível registrar o pagamento');
                        } else {
                            logger.info(`Pagamento ${result.insertId} criado.`);
                            res.location(`${req.url}/${result.insertId}`);

                            var response = {
                                dados_do_pagamento: pagamento,

                                links: [
                                    {
                                        href: res.get("location"),
                                        rel: "confirmar",
                                        method: "PUT"
                                    },
                                    {
                                        href: res.get("location"),
                                        rel: "cancelar",
                                        method: "DELETE"
                                    },
                                ]
                            }


                            res.status(201).json(response);
                        }
                    })
                }
            }
        }).catch(err => logger.error(err));
    });
}