var cache = require("../servicos/memcachedClient.js")();

function PagamentoDao(connection) {
    this._connection = connection;
    this._cache = cache();
}

PagamentoDao.prototype.salva = function (pagamento, callback) {
    this._connection.query('INSERT INTO pagamentos SET ?', pagamento, (err, result) => {
        callback(err, result);
        pagamento.id = result.insertId;
        this._cache.set(`pagamento-${result.insertId}`, pagamento, 15, function (err) {
            if (!err) {
                console.log(`Pagamento salvo no memcached: pagamento-${result.insertId}.`);
            }
        });
    });
}

PagamentoDao.prototype.atualiza = function (pagamento, callback) {
    this._connection.query('UPDATE pagamentos SET status = ? WHERE id = ?',
        [pagamento.status, pagamento.id], callback);
}

PagamentoDao.prototype.lista = function (callback) {
    this._connection.query('select * from pagamentos', callback);
}

PagamentoDao.prototype.buscaPorId = function (id, callback) {
    this._cache.get(`pagamento-${id}`, (err, cachedItem) => {
        if (!err && cachedItem) {
            console.log(`Pagamento pagamento-${cachedItem.id} encontrado no memcached.`);
            callback(err, [cachedItem]);
        } else {
            this._connection.query("select * from pagamentos where id = ?", [id], callback);
        }
    });
}

module.exports = function () {
    return PagamentoDao;
};