var fs = require("fs");
var file = process.argv[2];
fs.createReadStream(file)
    .pipe(fs.createWriteStream("../files/stream.jpg"))
    .on("finish", function(err) {
        if (err) {
            console.error(err);
        } else {
            console.log("Arquivo copiado");
        }
    });