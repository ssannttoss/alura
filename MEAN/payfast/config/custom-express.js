var express = require("express");
var consign = require("consign"); // evolução ao express-loader
var bodyParser = require("body-parser");
var expressValidator = require("express-validator");
var morgan = require("morgan");
var logger = require("../servicos/logger.js");

module.exports = function() {
    let app = express();
    app.use(morgan("common", {
        stream: {
            write: function(message) {
                logger.info(message);
            }
        }
    }))
    //app.use(bodyParser.urlencoded({ extend: true }));
    app.use(bodyParser.json());
    app.use(expressValidator({
        // errorFormatter
        errorFormatter: function(param, msg, value) {
            var namespace = param.split('.'),
                root = namespace.shift(), //remove e retorna o primeiro elemento do array
                formParam = root;

            while(namespace.length) {
                formParam += `[${namespace.shift()}]`;
            }

            return {
                param: formParam.toUpperCase(),
                msg: msg,
                value: value
            }
        },

        customValidators: {
            max: function(value, maxValue) {
                if (isNaN(value)) {
                    return value.length <= maxValue;
                } else {
                    return parseInt(value) < maxValue;
                }
            }
        }
    })); //Obrigatoriamente após o parser
    consign({extensions: ['.js']})
        .include("controllers")
        .then("servicos")
        .then("persistencia")
        .then("tests")
        .into(app);

    return app;
}