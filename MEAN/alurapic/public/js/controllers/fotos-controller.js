angular.module("alurapic").controller("FotosController", function($scope, recursoFoto) {
    $scope.fotos = [];
    $scope.filtro = "";
    $scope.mensagem = "";

    /**
     * The $http service is a core AngularJS service that facilitates communication
     * with the remote HTTP servers via the browser's XMLHttpRequest object or via JSONP.
     */

    /**
     * http://www.codelord.net/2015/05/25/dont-use-%24https-success/
     */
    // $http.get("v1/fotos").success((data) => $scope.fotos = data)
    //                      .error((err) => Console.error(err));

    // let promisse = $http.get("v1/fotos");
    // promisse.then((response) => $scope.fotos = response.data)
    //         .catch((err) => console.error(err));

    recursoFoto.query((data) => $scope.fotos = data, (err) => console.error(err));

    // ng-repeat="funcionario in funcionarios | filter: {nome: textoFiltro} "
    $scope.filtrar = function(foto) {
        return $scope.filtro == "" || foto.titulo.toLowerCase().indexOf($scope.filtro.toLowerCase()) > -1;
    };

    $scope.remover = function(foto) {
        // $http.delete(`v1/fotos/${foto._id}`).then((response) => {
        //     $scope.mensagem = `Foto '${foto.titulo}' removida.`;
        //     let indexOf = $scope.fotos.indexOf(foto);
        //     $scope.fotos.splice(indexOf, 1);

        // }).catch((err) => {
        //     console.error(err);
        //     $scope.mensagem = `Falha ao remover a foto '${foto.titulo}.'`;
        // });
        recursoFoto.delete({fotoId: foto._id}, () => {
            $scope.mensagem = `Foto '${foto.titulo}' removida.`;
            let indexOf = $scope.fotos.indexOf(foto);
            $scope.fotos.splice(indexOf, 1);
        }, (err) => {
            console.error(err);
            $scope.mensagem = `Falha ao remover a foto '${foto.titulo}.'`;
        });
    };
});