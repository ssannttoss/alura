angular.module("alurapic").controller("FotoController",
    // Angular possui o annotation system para resolver problemas com mudança de nomes
    // de variáveis em proceso de minificação
    // São criados "alias" para os parâmetros e este não são alterados
    ["$scope", "recursoFoto", "cadastroFotos", "$routeParams",
    function ($scope, recursoFoto, cadastroFotos, $routeParams) {
    $scope.foto = {};
    $scope.mensagem = "";

    if ($routeParams.fotoId) {
        // let url = `v1/fotos/${$routeParams.fotoId}`;
        // $http.get(url).then((result) => {
        //     $scope.foto = result.data;
        // }).catch((err) => {
        //     $scope.mensagem = "Não foi possível obter a foto.";
        //     console.error(err);
        // });
        recursoFoto.get({fotoId: $routeParams.fotoId}, (data) => $scope.foto = data,
            (err) => {
                console.error(err);
                $scope.mensagem = "Não foi possível obter a foto.";
            });
    }

    $scope.submeter = function () {
        if ($scope.formulario.$valid) {
            cadastroFotos.cadastrar($scope.foto).then((result) => {
                $scope.mensagem = result.mensagem;
                $scope.focado = true;

                if (result.inclusao) {
                    $scope.foto = {};
                    //https://docs.angularjs.org/api/ng/type/form.FormController
                    // Seta o form pro estato original
                    $scope.formulario.$setPristine();
                }
            }).catch((err) => $scope.mensagem = err.mensagem);
        } else {
            if ($scope.formulario.url.$invalid) {
                console.error("URL inválida");
            }

            if ($scope.formulario.titulo.$invalid) {
                console.error("Título inválida");
            }
        }
    }

}]);