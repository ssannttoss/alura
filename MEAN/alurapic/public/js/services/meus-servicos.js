angular.module("meusServicos", ["ngResource"])
.factory("recursoFoto", function($resource) {
    return $resource("v1/fotos/:fotoId", null, {
        update: {
            method: 'PUT'
        }
    });
})
.factory("recursoGrupo", function($resource) {
    return $resource("v1/grupos", null, {
    })
})
/**
 * $q Serviço para criar promisses
 */
.factory("cadastroFotos", function(recursoFoto, $q, $rootScope) {
    let servico = {};
    let evento = "fotoCadastrada";
    servico.cadastrar = function(foto) {
        return $q(function(resolve, reject) {
            if (foto._id) {
                recursoFoto.update({fotoId: foto._id}, foto, () => {
                    $rootScope.$broadcast(evento);
                    resolve({mensagem: `Foto ${foto.titulo} atualizada com sucesso.`,
                        inclusao: false
                    });
                }, (err) => {
                    console.log(err);
                    reject({mensagem: `Não foi possível atualizar a foto ${foto.titulo}.`});
                });
            } else {
                recursoFoto.save(foto, () => {
                    $rootScope.$broadcast(evento);
                    resolve({mensagem: `Foto ${foto.titulo} incluída com sucesso.`,
                        inclusao: true
                    });
                }, (err)=> {
                    console.log(err);
                    reject({mensagem: `Não foi possível incluir a foto ${foto.titulo}.`});
                });
            }
        });
    };

    return servico;
});

/* https://docs.angularjs.org/api/ngResource/service/$resource
{ 'get':    {method:'GET'},
  'save':   {method:'POST'},
  'query':  {method:'GET', isArray:true},
  'remove': {method:'DELETE'},
  'delete': {method:'DELETE'} };
  */