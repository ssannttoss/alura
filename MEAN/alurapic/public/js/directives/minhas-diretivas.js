angular.module("minhasDiretivas", [])
.directive("meuPainel", function() {
    var ddo = {}; // directive definition object

    // A directive can specify which of the 4 matching types it supports in the restrict property of the directive definition object. The default is EA
    // can match directives based on element names (E), attributes (A), class names (C), and comments (M).
    ddo.restrict = "AEC"; // Attribute (<div meu-painel) or Element (<meu-painel></meu-painel>)
    // cameCase no nome da diretive camel-case no html

    ddo.scope = {
        // o scope é da diretiva
        titulo: "@titulo" //como o nome da inteface é o mesmo do atributo no escopo, poderia usar apoenas @
    }

    // Directive that marks the insertion point for the transcluded DOM of the nearest parent directive that uses transclusion.
    // https://docs.angularjs.org/api/ng/directive/ngTransclude
    ddo.transclude = true; // mantém os elementos filho.

    // ddo.template = `
    //     <div class="panel panel-default col-md-2">
    //         <div class="panel-heading">
    //             <h3 class="panel-title text-center">{{titulo}}</h3>
    //         </div>
    //         <div class="panel-body" ng-transclude>

    //         </div>
    //     </div> <!--Fim Panel-->
    // `;
    ddo.templateUrl = "js/directives/meu-painel.html";

    return ddo;
})
.directive("minhaFoto", function() {
    var ddo = {}; // directive definition object

    // A directive can specify which of the 4 matching types it supports in the restrict property of the directive definition object. The default is EA
    // can match directives based on element names (E), attributes (A), class names (C), and comments (M).
    ddo.restrict = "AEC"; // Attribute (<div meu-painel) or Element (<meu-painel></meu-painel>)
    // cameCase no nome da diretive camel-case no html

    ddo.scope = {
        // o scope é da diretiva
        titulo: "@",
        url: "@"
    }

    // Directive that marks the insertion point for the transcluded DOM of the nearest parent directive that uses transclusion.
    // https://docs.angularjs.org/api/ng/directive/ngTransclude
    ddo.transclude = false; // mantém os elementos filho.
    ddo.templateUrl = "js/directives/minha-foto.html";

    return ddo;
})
.directive("meuBotaoPerigo", function() {
    let ddo = {};
    ddo.directive = "E";

    ddo.scope = {
        nome: "@",
        acao: "&" // Usar & quando queremos passar uma expressão para ser avalida dentro da diretiva
    };
    ddo.template = `<button ng-click="acao(param)" class="btn btn-danger btn-block">{{nome}}</button>`;

    return ddo;
})
.directive("meuFoco", function() {
    let ddo = {};
    ddo.directive = "A";

    ddo.scope = {
        focado: "="
    };


    ddo.link = function(scope, element) {
        // scope.$watch("focado", function() {
        //     if (scope.focado) {

        // $watcher, o "oráculo" do data binding do Angular, mas não podemos abusar dele em
        // nossa diretivas, porque há um custo computacional nisso, o que pode tornar a
        // atualização de nossa view bem lenta.
        //         /**
        //         * O element é um elemento DOM, porém encapsulado pelo jqLite,
        //         * É uma API de manipulação de DOM usada pelo Angular que tenta seguir o padrão da
        //         * API Sizzle, mesmo padrão usando pelo jQuery, jbMobi ou Zepto.
        //         * O problema é que o jqLite não possui a função .focus() que o jQuery possui.
        //         * Neste caso, quando usamos element[0] significa que estamos acessando diretamente
        //         * o elemento do DOM encapsulado pelo jqLite, sendo assim, podemos chamar a
        //         * função focus() diretamente no elemento.
        //         */
        //         element[0].focus();
        //         scope.focado = false;
        //     }
        // });

        scope.$on('fotoCadastrada', () => element[0].focus());
    };

    return ddo;
})
.directive("meusTitulos", function() {
    let ddo = {};
    ddo.restrict = "E";
    ddo.template = `<ul><li ng-repeat="titulo in titulos">{{titulo}}</li></ul>`;

    ddo.controller = function($scope, recursoFoto) {
        recursoFoto.query(function(fotos) {
            $scope.titulos = fotos.map((foto) => foto.titulo);
        });
    };

    return ddo;
});