// realiza update adicionado um item no array
db.alunos.update(
       { "_id": ObjectId("59614482f168bab20bdfdb9d") },
       { $push: { "notas" : 8.5 } }
);
       
db.alunos.find({ "_id": ObjectId("59614482f168bab20bdfdb9d") });
           
// realiza update adicionando v�rios itens no arry. N�o usar { $push: { "notas" : [7, 3.5] } }
db.alunos.update(
       { "_id": ObjectId("59614482f168bab20bdfdb9d") },
       { $push: { "notas" : { $each: [7, 3.5] } } }
);   

db.alunos.find({ "_id": ObjectId("59614482f168bab20bdfdb9d") });       
           