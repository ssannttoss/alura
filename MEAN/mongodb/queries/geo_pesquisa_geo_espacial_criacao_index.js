// criando a cole��o
db.createCollection("alunos_geo");

// para pesquisar por localiza��o � necess�rio a cria��o de um �ndice
db.alunos_geo.createIndex({
    localizacao : "2dsphere" //duas dimens�es
});

db.alunos_geo.aggregate([
{
    $geoNear : { 
        near : { // fun��o near
            coordinates: [-23.5640265, -46.6527128],
            type : "Point"
        },
        distanceField : "distancia.calculada", // vari�vel para armazenar a dist�ncia
        spherical : true, // forma de pesquisa
        limit : 4 // limit
    }
}, 
{
    $skip: 1 // Remove o primeiro registro uma vez que o mesmo foi usado como ponto base.
}   
]);