db.alunos.find().forEach((doc) => {
        db.alunos.update( 
            { "_id": doc._id },
            { "nome": doc.nome.toUpperCase() }
        );
});

var id;
db.alunos.find({ "nome": "FELIPE" }).forEach((doc) => {
    if (id) {
        return;
    };
    
    id = doc._id;
    db.alunos.update(
        { "_id": doc._id },
        { $push : { "notas": { $each: [8.8, 0.1] } } }        
    ); 
});

db.alunos.findOne({ "nome": "FELIPE" });