let mongoose = require("mongoose");
let schema = mongoose.Schema({
    titulo: {
        type: String,
        required: true
    },

    url: {
        type: String,
        required: true
    },

    descricao: {
        type: String,
        required: true
    },

    grupo: {
        type: Number,
        require: false
    }
});

mongoose.model("Foto", schema);
