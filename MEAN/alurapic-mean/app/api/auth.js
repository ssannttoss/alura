var mongoose = require("mongoose");
var jwt = require("jsonwebtoken");

module.exports = function(app) {
    var api = {};
    var model = mongoose.model("Usuario");
    api.autentica = function(req, res) {
        model.findOne({
            login: req.body.login,
            senha: req.body.senha
        }).then(function(usuario) {
            if (!usuario) {
                console.error(`${req.body}: Login/Senha inválidos`);
                res.sendStatus(401);
            } else {
                try {
                    var token = jwt.sign({login: usuario.login}, app.get("secret"), {
                        expiresIn: 30 // valor em segundos
                    });
                    console.info(`${req.body.login} autenticado.`);
                    res.set("x-access-token", token);  // adicionando token no cabeçalho de resposta
                    res.end(); // enviando a resposta
                } catch(e) {
                    console.error(e);
                    res.sendStatus(500).json(e);
                }
            }
        }, function(err) {
            console.err(err);
            res.sendStatus(401);
        });
    };

    api.verificaToken = function(req, res, next) {
        var token = req.headers["x-access-token"]; // busca o token no header da requisição

        if (token) {
            console.info("Token recebido. Decodificando...");
            jwt.verify(token, app.get("secret"), function(err, decoded) {
                if (err) {
                    console.error("Token rejeitado:" + err);
                    return res.sendStatus(401);
                } else {
                    console.log("Token aceito");
                    req.usuario = decoded;
                    next();
                }
            });
        } else {
            console.warn("Nenhum token enviado");
            return res.sendStatus(401);
        }
    };

    return api;
};