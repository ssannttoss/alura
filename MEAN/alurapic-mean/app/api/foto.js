let mongoose = require("mongoose");
let api = {};
let model = mongoose.model("Foto");

api.lista = (req, res) => {
    model.find()
        .then((fotos) => {
            res.json(fotos);
        }, (err) => {
            console.error(err);
            res.sendStatus(500);
        });
};

api.buscaPorId = (req, res) => {
    model.findById(req.params.id)
        .then((foto) => {
            if (foto) {
                res.json(foto);
            } else {
                console.warn(`Foto ${req.params.id} não encontrada`);
                res.sendStatus(404);
            }
        }, (err) => {
            console.error(err);
            res.sendStatus(500).json(err);
        });
};

api.removePorId = (req, res) => {
    model.findByIdAndRemove(req.params.id)
        .then((foto) => {
            console.info(`Foto removida: ${req.params.id}`)
            res.sendStatus(204);
        }, (err) => {
            console.error(err);
            res.sendStatus(500).json(err);
        });
};

api.adiciona = (req, res) => {
    model.create(req.body)
        .then((foto) => {
            console.info(`Foto adicionada: ${JSON.stringify(req.body)}`)
            res.sendStatus(201).json(foto);
        }, (err) => {
            console.error(err);
            res.sendStatus(500).json(err);
        });
};

api.atualiza = (req, res) => {
    model.findByIdAndUpdate(req.params.id, req.body)
        .then((foto) => {
            console.info(`Foto atualizada: ${JSON.stringify(foto)}`);
            res.sendStatus(200);
        }, (err) => {
            console.error(err);
            res.sendStatus(500).json(err);
        });
}

module.exports = api;