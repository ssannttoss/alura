module.exports = function(uri) {
    var mongoose = require("mongoose");
    mongoose.connect("mongodb://" + uri);

    mongoose.connection.on("connected", () => {
        console.info("App is connected to Mongodb.");
    });

    mongoose.connection.on("disconnected", () => {
        console.info("App is disconnected from Mongodb");
    });

    mongoose.connection.on("error", (err) => {
        console.error("Connection failed. Error: " + err);
    })

    process.on("SIGINT", () => {
        mongoose.connection.close((err) => {
            if (err) {
                console.error("Failed to close Mongodb connection. Error: " + err);
            } else {
                console.info("Mongodb connection has been closed");
            }

            process.exit(0);
        });
    });
}