let consign = require("consign");
let express = require("express");
let bodyParser = require("body-parser");

let app = express();

app.set("secret", "senha");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.use(express.static("./public"));

consign({cwd: "app"})
    .include("models")
    .then("api")
    .then("routes/auth.js")
    .then("routes")
    .into(app);

module.exports = app;